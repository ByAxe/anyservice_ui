import 'package:anyserviceui/bearer/spinner_state_bearer.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Spinner toggles', () {
    final SpinnerStateBearer spinnerService = SpinnerStateBearer();

    // Get basic spinner state
    final bool spinnerBasicState = spinnerService.isShowSpinner;

    // Toggle spinner state
    spinnerService.toggleSpinner();

    // Get value after toggling
    final bool afterToggle = spinnerService.isShowSpinner;

    // Compare that it is changed
    expect(!afterToggle, spinnerBasicState);

    // Toggle spinner state again
    spinnerService.toggleSpinner();

    final bool afterOneMoreToggle = spinnerService.isShowSpinner;

    expect(afterOneMoreToggle, spinnerBasicState);
  });
}
