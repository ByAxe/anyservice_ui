import 'dart:developer' as developer;

import 'package:anyserviceui/model/constants/keys.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:random_string/random_string.dart';
import 'package:test/test.dart';
import 'package:uuid/uuid.dart';

import 'resources.dart';
import 'utility_methods.dart';

void main() {
  group('AnyService App', () {
    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();

      // Create directory for screenshots
      await createDirectory(Resources.screenshotsFolder);
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('check flutter driver health', () async {
      final Health health = await driver.checkHealth();
      developer.log(health.status.toString());
    });

    /// --------------------------------------------------
    /// REGISTER USER TEST
    /// --------------------------------------------------
    const String registerUserTest = 'register_user_test';
    test(registerUserTest, () async {
      // Path to screenshots
      final String p = '${Resources.screenshotsFolder}/$registerUserTest';
      createDirectory(p);

      // Screenshot counter
      int c = 0;

      // initial_state
      await takeScreenshot(driver, '$p/${c++}_init.png');
      await driver.tap(find.byValueKey(Keys.registrationTextButton));
      await takeScreenshot(driver, '$p/${c++}_registration_screen.png');

      const String name = 'Test Testovich';
      final String email = '${Uuid().v4().substring(0, 5)}-test@email.com';
      final String phone =
          '+${randomBetween(10, 999)}${randomBetween(10, 999)}${randomBetween(100, 999)}${randomBetween(100, 999)}';
      final String password = '${Uuid().v4()}-test';

      await enterTextToTextField(
          find.byValueKey(Keys.registrationName), name, driver);
      await enterTextToTextField(
          find.byValueKey(Keys.registrationEmail), email, driver);
      await enterTextToTextField(
          find.byValueKey(Keys.registrationPhone), phone, driver);
      await enterTextToTextField(
          find.byValueKey(Keys.registrationPassword), password, driver);
      await takeScreenshot(driver, '$p/${c++}_values_entered.png');
      await driver.tap(find.byValueKey(Keys.registrationRegisterButton));

      final SerializableFinder profile = find.text('Profile');
      await driver.waitFor(profile);
      await takeScreenshot(driver, '$p/${c++}_registration_passed.png');

      await driver.tap(profile);
      final SerializableFinder settingsButton =
          find.byValueKey(Keys.settingsMenuButton);
      await driver.waitFor(settingsButton);
      await takeScreenshot(driver, '$p/${c++}_profile_screen.png');

      await driver.tap(find.byValueKey(Keys.settingsMenuButton));
      await takeScreenshot(driver, '$p/${c++}_settings_screen.png');

      // Tap on logout
      final SerializableFinder logoutButton =
          find.byValueKey(Keys.logoutButton);
      await driver.tap(logoutButton);

      // Make sure that we logged out
      await driver.waitForAbsent(logoutButton);
      await takeScreenshot(driver, '$p/${c++}_logged_out.png');
    });

    /// --------------------------------------------------
    /// SIGN IN USER TEST
    /// --------------------------------------------------
    const String signInSignOutTest = 'sign_in_sign_out';
    test(signInSignOutTest, () async {
      // Path to screenshots
      final String p = '${Resources.screenshotsFolder}/$signInSignOutTest';
      createDirectory(p);

      // Screenshot counter
      int c = 0;

      // initial_state
      await takeScreenshot(driver, '$p/${c++}_init.png');

      // Go to Profile screen
      final SerializableFinder profile = find.text('Profile');
      final SerializableFinder settings =
          find.byValueKey(Keys.settingsMenuButton);
      final SerializableFinder authLoginButton =
          find.byValueKey(Keys.authLoginButton);
      final SerializableFinder logoutButton =
          find.byValueKey(Keys.logoutButton);

      final bool startedAsLoggedIn = await isPresent(profile, driver);

      if (startedAsLoggedIn) {
        driver.tap(profile);
        await takeScreenshot(driver, '$p/${c++}_profile_screen.png');

        await driver.tap(settings);
        await takeScreenshot(driver, '$p/${c++}_settings_screen.png');

        // Tap on logout
        await driver.tap(logoutButton);

        // Make sure that we logged out
        await driver.waitFor(authLoginButton);
        await takeScreenshot(driver, '$p/${c++}_logged_out.png');
      }

      // Tap on login button
      await driver.tap(authLoginButton);
      await takeScreenshot(driver, '$p/${c++}_login_screen.png');

      // Wait until login screen loads
      final SerializableFinder loginButton = find.byValueKey(Keys.loginButton);
      await driver.waitFor(loginButton);

      // Get value fields
      final SerializableFinder emailField = find.byValueKey(Keys.loginEmail);
      final SerializableFinder passField = find.byValueKey(Keys.loginPassword);

      // Enter text to fields
      enterTextToTextField(emailField, Resources.testEmail, driver);
      await takeScreenshot(driver, '$p/${c++}_email_entered.png');

      enterTextToTextField(passField, Resources.testPassword, driver);
      await takeScreenshot(driver, '$p/${c++}_pass_entered.png');

      // Login with entered credentials
      await driver.tap(loginButton);
      await takeScreenshot(driver, '$p/${c++}_login_tapped.png');

      // Wait for main screen loads and tap on Profile
      final bool isOnProfileScreen = await isPresent(settings, driver);

      if (!isOnProfileScreen) {
        await driver.tap(profile);
        await takeScreenshot(driver, '$p/${c++}_logged_in.png');
      }

      // Go to settings
      await driver.tap(settings);
      await takeScreenshot(driver, '$p/${c++}_settings_screen.png');

      // Main screen
      final SerializableFinder settingsCloseButton =
          find.byValueKey(Keys.settingsCloseButton);
      await driver.tap(settingsCloseButton);
      await takeScreenshot(driver, '$p/${c++}_home_screen.png');
    });

    /// --------------------------------------------------
    /// CREATE ORDER TESTS
    /// --------------------------------------------------
    const String createOrderTest = 'create_order_test';
    for (final OrderCreationTestCase testCase in OrderCreationTestCase.values) {
      test(createOrderTest, () async {
        // Path to screenshots
        final String p =
            '${Resources.screenshotsFolder}/${createOrderTest}_$testCase';
        createDirectory(p);

        // Screenshot counter
        int c = 0;

        // initial_state
        await takeScreenshot(driver, '$p/${c++}_init.png');

        // Go to order creation screen
        final SerializableFinder fab = find.byValueKey(Keys.fab);
        driver.tap(fab);
        await takeScreenshot(driver, '$p/${c++}_order_creation_screen.png');

        final SerializableFinder imagePickerBox =
            find.byValueKey(Keys.imagePickerBox);
        final SerializableFinder headline =
            find.byValueKey(Keys.orderCreationHeadlineField);
        final SerializableFinder phoneNumber =
            find.byValueKey(Keys.orderCreationPhoneNumber);
        final SerializableFinder category =
            find.byValueKey(Keys.orderCreationCategoryField);
        final SerializableFinder deadline =
            find.byValueKey(Keys.orderCreationDeadlineField);
        final SerializableFinder locationType =
            find.byValueKey(Keys.orderCreationLocationTypeField);
        final SerializableFinder address =
            find.byValueKey(Keys.orderCreationAddressField);
        final SerializableFinder price =
            find.byValueKey(Keys.orderCreationPriceField);
        final SerializableFinder discussablePrice =
            find.byValueKey(Keys.orderCreationDiscussablePriceCheckBox);
        final SerializableFinder description =
            find.byValueKey(Keys.orderCreationDescriptionField);
        final SerializableFinder orderCreationButton =
            find.byValueKey(Keys.orderCreationButton);
        final SerializableFinder currencyPicker =
            find.byValueKey(Keys.orderCreationCurrencyPicker);

        await enterTextToTextField(headline, 'Test order headline', driver);

        await driver.tap(category);
        await driver.tap(find.text('Cleaning'));
        final SerializableFinder categoriesModalAcceptButton =
            find.byValueKey(Keys.orderCreationCategoriesModalAcceptButton);
        await driver.tap(categoriesModalAcceptButton);

        await enterTextToTextField(phoneNumber, '+48999999999', driver);

        await driver.tap(deadline);
        await driver.tap(find.text('Confirm'));

        await driver.tap(locationType);
        final SerializableFinder locationTypeConfirmButton =
            find.text('Confirm');
        await driver.tap(locationTypeConfirmButton);

        await driver.scrollUntilVisible(find.byType('ListView'), description,
            dyScroll: -300);

        await enterTextToTextField(address, 'Some address', driver);

        if (testCase == OrderCreationTestCase.FIXED_PRICE) {
          await driver.tap(currencyPicker);
          await driver.tap(find.text('Confirm'));
          await enterTextToTextField(price, '14.0', driver);
        } else if (testCase == OrderCreationTestCase.DISCUSSABLE_PRICE) {
          await driver.tap(discussablePrice);
        }

        await enterTextToTextField(description, 'Some more details', driver);
        await takeScreenshot(driver, '$p/${c++}_fields_filled.png');
        await driver.tap(orderCreationButton);

        // Make sure that we created our order
        await driver.waitFor(fab);
        await takeScreenshot(driver, '$p/${c++}_order_created.png');
      });
    }
  });
}

/// Test cases for order creation
enum OrderCreationTestCase {
  FIXED_PRICE,
  DISCUSSABLE_PRICE,
  // WITH_IMAGES,
}
