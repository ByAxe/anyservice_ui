class Resources {
  static String testEmail = 'somemail@mail.com';
  static String testPassword = '12345678';

  static String screenshotsFolder = 'screenshots';

  /// Flag that enables screenshots saving in tests
  static bool screenshotsEnabled = true;
}
