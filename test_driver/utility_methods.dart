import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';

import 'resources.dart';

/// Taking screenshot and saving into the given pass
Future<void> takeScreenshot(FlutterDriver driver, String path) async {
  // If screenshots are not enabled - do nothing
  if (!Resources.screenshotsEnabled) return;

  final List<int> pixels = await driver.screenshot();
  final File file = File(path);
  await file.writeAsBytes(pixels);
}

/// Create directory for this path
Future<void> createDirectory(String path) async {
  // If screenshots are not enabled - do nothing
  if (!Resources.screenshotsEnabled) return;

  Directory(path).create();
}

/// Utility method for creation of delay in tests
Future<void> delay([int milliseconds = 250]) async {
  await Future<void>.delayed(Duration(milliseconds: milliseconds));
}

/// Utility method for entering text to some text field
///
/// Tap on field
/// Enter text
/// Wait until it appears
Future<void> enterTextToTextField(
  SerializableFinder field,
  String text,
  FlutterDriver driver,
) async {
  await driver.tap(field);
  await driver.enterText(text);
  await driver.waitFor(find.text(text));
}

Future<bool> isPresent(
  SerializableFinder byValueKey,
  FlutterDriver driver, {
  Duration timeout = const Duration(seconds: 1),
}) async {
  try {
    await driver.waitFor(byValueKey, timeout: timeout);
    return true;
  } catch (exception) {
    return false;
  }
}
