import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/screen/customer/new_order/components/currency_picker.dart';
import 'package:anyserviceui/screen/customer/new_order/components/discussable_price_checkbox.dart';
import 'package:anyserviceui/screen/customer/new_order/components/field.dart';
import 'package:anyserviceui/service/validators/order_validator.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderCreationPriceBlock extends StatelessWidget {
  const OrderCreationPriceBlock({
    Key key,
    @required this.order,
  }) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width * 0.9;

    final OrderCreationBearer priceBearer =
        Provider.of<OrderCreationBearer>(context);

    final bool isDiscussablePrice = priceBearer.isDiscussablePrice;
    final TextEditingController priceController = priceBearer.priceController;

    return Container(
      width: width,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              CurrencyPicker(
                order: order,
                width: width * 0.22,
                key: Key(Keys.orderCreationCurrencyPicker),
              ),
              OrderCreationField(
                isEnabled: !isDiscussablePrice,
                controller: priceController,
                hintText: 'Price',
                keyboard: const TextInputType.numberWithOptions(decimal: true),
                onChange: (String price) => Provider.of<OrderCreationBearer>(
                  context,
                  listen: false,
                ).setPrice(price),
                validator: (String price) => OrderValidator.validatePrice(
                  price,
                  isDiscussablePrice: isDiscussablePrice,
                ),
                key: Key(Keys.orderCreationPriceField),
                width: width * 0.75,
              ),
            ],
          ),
          DiscussablePriceCheckbox(
            isDiscussablePrice: isDiscussablePrice,
            priceController: priceController,
          ),
        ],
      ),
    );
  }
}
