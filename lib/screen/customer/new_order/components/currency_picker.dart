import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/component/common/basic_picker.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:provider/provider.dart';

class CurrencyPicker extends StatelessWidget {
  const CurrencyPicker({
    Key key,
    this.order,
    this.width,
  }) : super(key: key);

  final Order order;
  final double width;

  @override
  Widget build(BuildContext context) {
    final List<String> pickerData =
        Currency.values.map((Currency c) => c.toShortString()).toList();

    return Consumer<OrderCreationBearer>(
      builder: (BuildContext c, OrderCreationBearer b, _) {
        final Currency currency = b.currency;
        final Color textColor = (b.price == null || b.price.isEmpty)
            ? kDividerColor
            : Colors.black54;

        return BasicPicker(
          width: width,
          textStyle: TextStyle(color: textColor, fontSize: 15.0),
          fieldValue: currency.toShortString(),
          pickerData: pickerData,
          onConfirm: (Picker picker, _) {
            // Get text from picker and get value between '[' and ']'
            final String visibleText = picker.adapter.text;
            final String text =
                visibleText.substring(1, visibleText.length - 1);

            // Get chosen location type
            final Currency newCurrency = Currency.values
                .where((Currency c) => c.toShortString() == text)
                .first;

            // Update current location type
            Provider.of<OrderCreationBearer>(c, listen: false)
                .setCurrency(newCurrency);

            // Set updated currency to object-bearer
            order.currency = newCurrency;
          },
        );
      },
    );
  }
}
