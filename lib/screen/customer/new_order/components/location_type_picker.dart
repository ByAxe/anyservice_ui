import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/component/common/basic_picker.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/dto/location.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class LocationTypePicker extends StatelessWidget {
  const LocationTypePicker({
    Key key,
    @required this.order,
  }) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width * 0.9;

    final List<String> pickerData = LocationTypeDescriptionMap.entries
        .skip(1)
        .map((MapEntry<LocationType, String> e) => e.value)
        .toList();

    return Consumer<OrderCreationBearer>(
      builder: (BuildContext c, OrderCreationBearer b, _) {
        final LocationType locationType = b.locationType;
        final String currentFieldValue =
            LocationTypeDescriptionMap[b.locationType];
        final Color textColor =
            locationType == null ? kDividerColor : Colors.black54;

        return BasicPicker(
          width: width,
          textStyle: TextStyle(color: textColor, fontSize: 15.0),
          fieldValue: currentFieldValue,
          pickerData: pickerData,
          onConfirm: (Picker picker, _) {
            // Get text from picker and get value between '[' and ']'
            final String visibleText = picker.adapter.text;
            final String text =
                visibleText.substring(1, visibleText.length - 1);

            // Get chosen location type
            final LocationType newType = LocationTypeDescriptionMap.entries
                .where((MapEntry<LocationType, String> e) => e.value == text)
                .map((MapEntry<LocationType, String> e) => e.key)
                .first;

            // Update current location type
            Provider.of<OrderCreationBearer>(c, listen: false)
                .setLocationType(newType);

            // Set updated location type to object-bearer
            order.location.locationType = locationType;
          },
          rightmostWidget: getRightmostWidget(c, locationType),
        );
      },
    );
  }

  /// Get rightmost widget visible to user
  ///
  /// Can be either some hint icon for field type or analogue of clear-field-button
  Widget getRightmostWidget(BuildContext context, LocationType locationType) {
    if (locationType == null) {
      return const Icon(
        FontAwesomeIcons.caretDown,
        size: 15.0,
        color: kDividerColor,
      );
    }

    return GestureDetector(
      onTap: () {
        Provider.of<OrderCreationBearer>(context, listen: false)
            .setLocationType(null);

        order.location.locationType = locationType;
        order.location.addressLine1 = null;
      },
      child: const Icon(
        FontAwesomeIcons.timesCircle,
        size: 15.0,
      ),
    );
  }
}
