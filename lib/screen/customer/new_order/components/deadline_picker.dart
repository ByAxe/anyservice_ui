import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/component/common/datetime_picker.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:provider/provider.dart';

class DeadlinePicker extends StatelessWidget {
  const DeadlinePicker({
    Key key,
    @required this.order,
  }) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderCreationBearer>(
      builder: (BuildContext c, OrderCreationBearer b, _) {
        final DateTime deadline = b.dateTime;

        return DateTimePicker(
          fieldValue: deadline,
          minValue: DateTime.now(),
          textStyle: TextStyle(
            fontSize: 15.0,
            color: deadline == null ? kDividerColor : Colors.black54,
          ),
          onConfirm: (Picker picker, List<int> value) {
            // Get chosen deadline
            final DateTime newDeadline =
                (picker.adapter as DateTimePickerAdapter).value;

            // Update current deadline value
            Provider.of<OrderCreationBearer>(context, listen: false)
                .setDeadline(newDeadline);

            // Set updated deadline to object-bearer
            order.deadline = deadline;
          },
          onClearButtonTap: () {
            Provider.of<OrderCreationBearer>(context, listen: false)
                .setDeadline(null);
            order.deadline = deadline;
          },
        );
      },
    );
  }
}
