import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/screen/customer/new_order/components/address_field.dart';
import 'package:anyserviceui/screen/customer/new_order/components/category_picker.dart';
import 'package:anyserviceui/screen/customer/new_order/components/deadline_picker.dart';
import 'package:anyserviceui/screen/customer/new_order/components/field.dart';
import 'package:anyserviceui/screen/customer/new_order/components/location_type_picker.dart';
import 'package:anyserviceui/screen/customer/new_order/components/phone_field.dart';
import 'package:anyserviceui/screen/customer/new_order/components/price_block.dart';
import 'package:anyserviceui/service/validators/order_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderCreationForm extends StatefulWidget {
  const OrderCreationForm({Key key, this.order, this.formKey})
      : super(key: key);

  final GlobalKey<FormState> formKey;
  final Order order;

  @override
  _OrderCreationFormState createState() => _OrderCreationFormState();
}

class _OrderCreationFormState extends State<OrderCreationForm> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      child: Column(
        children: <Widget>[
          OrderCreationField(
            hintText: 'Shortly about an order',
            onChange: (String name) => widget.order.headline = name,
            validator: OrderValidator.validateShortDescription,
            key: Key(Keys.orderCreationHeadlineField),
          ),
          CategoryPicker(
            order: widget.order,
            key: Key(Keys.orderCreationCategoryField),
          ),
          PhoneField(order: widget.order),
          DeadlinePicker(
            order: widget.order,
            key: Key(Keys.orderCreationDeadlineField),
          ),
          LocationTypePicker(
            order: widget.order,
            key: Key(Keys.orderCreationLocationTypeField),
          ),
          OrderCreationAddressField(
            order: widget.order,
            key: Key(Keys.orderCreationAddressField),
          ),
          OrderCreationPriceBlock(order: widget.order),
          OrderCreationField(
            hintText: 'Describe your order in details',
            keyboard: TextInputType.multiline,
            onChange: (String description) =>
                widget.order.description = description,
            validator: OrderValidator.validateDescription,
            minLines: 7,
            maxLines: 20,
            key: Key(Keys.orderCreationDescriptionField),
          ),
        ],
      ),
    );
  }
}
