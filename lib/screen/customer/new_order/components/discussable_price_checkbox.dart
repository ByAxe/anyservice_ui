import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DiscussablePriceCheckbox extends StatelessWidget {
  const DiscussablePriceCheckbox({
    Key key,
    @required this.isDiscussablePrice,
    @required this.priceController,
  }) : super(key: key);

  final bool isDiscussablePrice;
  final TextEditingController priceController;

  @override
  Widget build(BuildContext context) {
    final Color discussablePriceTextColor =
        isDiscussablePrice ? Colors.black54 : kDividerColor;

    return Row(
      children: <Widget>[
        Checkbox(
          value: isDiscussablePrice,
          onChanged: (bool isChecked) {
            final OrderCreationBearer bearer =
                Provider.of<OrderCreationBearer>(context, listen: false);

            bearer.setDiscussablePrice(isDiscussablePrice: isChecked);

            // If checkbox is checked - clear entered price
            if (isChecked) {
              priceController.clear();
              bearer.setPrice(null);
            }
          },
          key: Key(Keys.orderCreationDiscussablePriceCheckBox),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Discussable price',
              style:
                  TextStyle(color: discussablePriceTextColor, fontSize: 16.0),
              textAlign: TextAlign.start,
            ),
            Text(
              'This means you will not provide starting price',
              style: TextStyle(
                color: discussablePriceTextColor,
                fontSize: 12.0,
              ),
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      ],
    );
  }
}
