import 'package:anyserviceui/bearer/assets_bearer.dart';
import 'package:anyserviceui/component/common/rounded_button.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';

class ClearAttachmentButton extends StatelessWidget {
  const ClearAttachmentButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Asset> assets = Provider.of<AssetsBearer>(context).assets;
    final bool isVisible = assets != null && assets.isNotEmpty;

    return Visibility(
      visible: isVisible,
      child: RoundedButton(
        onPressed: () async {
          Provider.of<AssetsBearer>(context, listen: false).cleanAssets();
        },
        text: 'Clear attachments',
        color: kCustomerAccentColor,
      ),
    );
  }
}
