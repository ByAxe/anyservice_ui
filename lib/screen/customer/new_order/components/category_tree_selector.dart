import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/core/utils.dart';
import 'package:anyserviceui/model/constants/styles.dart';
import 'package:anyserviceui/model/dto/category.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:flutter/material.dart';
import 'package:flutter_treeview/tree_view.dart';
import 'package:provider/provider.dart';

class CategoryTreeSelector extends StatefulWidget {
  const CategoryTreeSelector({
    Key key,
    this.order,
    this.bearer,
  }) : super(key: key);

  final Order order;
  final OrderCreationBearer bearer;

  @override
  _CategoryTreeSelectorState createState() => _CategoryTreeSelectorState();
}

class _CategoryTreeSelectorState extends State<CategoryTreeSelector> {
  final bool _allowParentSelect = true;
  final bool _supportParentDoubleTap = true;

  TreeViewController controller = TreeViewController();

  @override
  Widget build(BuildContext context) {
    final List<Category> categories =
        Provider.of<OrderCreationBearer>(context).allCategories;

    List<Node> nodes = categoriesToNodes(categories);

    controller = controller.copyWith(children: nodes);

    return TreeView(
      controller: controller,
      allowParentSelect: _allowParentSelect,
      supportParentDoubleTap: _supportParentDoubleTap,
      onNodeTap: (String key) {
        final Category category = cast(getNode(key, nodes).data);

        Provider.of<OrderCreationBearer>(context, listen: false)
            .setCategory(category);

        widget.order.categories = <Category>[category];

        controller = controller.copyWith(selectedKey: key);
      },
      onExpansionChanged: (String key, bool expanded) {
        final Node node = getNode(key, nodes);

        if (node != null) {
          final List<Node> updated = controller
              .updateNode(key, node.copyWith(expanded: expanded))
              .cast<Node>();

          nodes = updated;

          controller = controller.copyWith(children: updated);
        }
      },
      theme: Styles.getTreeViewTheme(context),
    );
  }

  /// Converts list of [Category]'ies to List of [Node]'s with <Category> inside
  List<Node> categoriesToNodes(final List<Category> categories) {
    final List<Node> nodes = <Node<Category>>[];

    for (final Category category in categories) {
      final bool hasParent = category.parentCategory != null;
      final Node<Category> childNode = categoryToNode(category);

      if (hasParent) {
        final Category parentCategory = category.parentCategory;
        final String parentEntryKey = parentCategory.id;

        // Find parent entry in Map
        Node parentNode = getNode(parentEntryKey, nodes);

        if (parentNode != null) {
          // Add Child to [ ] of Parent Entry
          parentNode.children.add(childNode);
        } else {
          parentNode = categoryToNode(parentCategory);

          // Add Child to [ ] of Parent Entry
          parentNode.children.add(childNode);

          // Add Parent Entry to Map
          nodes.add(parentNode);
        }
      } else {
        // Add Entry to Map
        nodes.add(childNode);
      }
    }

    return nodes;
  }

  /// Converts [Category] to [Node] with <Category> inside
  Node<Category> categoryToNode(Category category) => Node<Category>(
        key: category.id,
        label: category.title,
        data: category,
        children: <Node<Category>>[],
      );

  /// Gets the node that has a key value equal to the specified key.
  Node getNode(String key, List<Node> children) {
    Node _found;
    final Iterator<Node> iter = children.iterator;
    while (iter.moveNext()) {
      final Node child = iter.current;
      if (child.key == key) {
        _found = child;
        break;
      } else {
        if (child.isParent) {
          _found = getNode(key, child.children);
          if (_found != null) {
            break;
          }
        }
      }
    }
    return _found;
  }
}
