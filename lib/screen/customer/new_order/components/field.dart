import 'package:anyserviceui/component/common/rounded_text_field.dart';
import 'package:anyserviceui/model/constants/styles.dart';
import 'package:flutter/material.dart';

class OrderCreationField extends StatelessWidget {
  const OrderCreationField({
    Key key,
    this.width,
    this.hintText,
    this.onChange,
    this.validator,
    this.keyboard,
    this.isEnabled,
    this.controller,
    this.minLines,
    this.maxLines,
  }) : super(key: key);

  final double width;
  final String hintText;
  final void Function(String) onChange;
  final String Function(String) validator;
  final TextInputType keyboard;
  final bool isEnabled;
  final TextEditingController controller;
  final int minLines;
  final int maxLines;

  @override
  Widget build(BuildContext context) {
    return RoundedTextField(
      isEnabled: isEnabled,
      controller: controller,
      minLines: minLines,
      maxLines: maxLines,
      textStyle: const TextStyle(color: Colors.black54),
      validator: validator,
      keyboard: keyboard,
      width: width ?? MediaQuery.of(context).size.width * 0.9,
      textAlign: TextAlign.start,
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      onChange: onChange,
      inputDecoration: Styles.getInputDecoration(hintText),
      boxDecoration: Styles.getBoxDecoration(),
    );
  }
}
