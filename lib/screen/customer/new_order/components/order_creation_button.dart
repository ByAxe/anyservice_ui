import 'package:anyserviceui/bearer/assets_bearer.dart';
import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/bearer/user_bearer.dart';
import 'package:anyserviceui/component/common/rounded_button.dart';
import 'package:anyserviceui/core/decimal_wrapper.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:anyserviceui/model/dto/file.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/service/crud/file_service.dart';
import 'package:anyserviceui/service/crud/order_service.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';

class OrderCreationButton extends StatelessWidget {
  const OrderCreationButton({
    Key key,
    this.formKey,
    this.order,
  }) : super(key: key);

  final GlobalKey<FormState> formKey;
  final Order order;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width * 0.9;

    return RoundedButton(
      minWidth: width,
      text: 'Create',
      color: kCustomerAccentColor,
      textStyle: kSingleButtonsTextStyle,
      onPressed: () async {
        if (!formKey.currentState.validate()) return;
        final Order order = await _createOrder(context);
        debugPrint(order?.toJson()?.toString());
        Navigator.pop(context, order);
      },
    );
  }

  Future<Order> _createOrder(BuildContext context) async {
    final OrderCreationBearer orderCreationBearer =
        Provider.of<OrderCreationBearer>(context, listen: false);
    final AssetsBearer assetsBearer =
        Provider.of<AssetsBearer>(context, listen: false);

    final FileService fileService =
        Provider.of<FileService>(context, listen: false);
    final OrderService orderService =
        Provider.of<OrderService>(context, listen: false);
    final UserBearer userBearer =
        Provider.of<UserBearer>(context, listen: false);

    order.customer = userBearer.user;
    order.price = DecimalWrapper.fromDynamic(orderCreationBearer.price);

    final List<Asset> assets = assetsBearer.assets;

    final List<FileMetadata> files = <FileMetadata>[];

    // Save assets to server storage on create order click
    for (int i = 0; i < assets.length; i++) {
      final Asset asset = assets[i];
      final FileMetadata fileMetadata = await fileService.saveImage(asset);

      files.add(fileMetadata);
    }

    if (files != null && files.isNotEmpty) {
      // Set list of FileMetadata to new order
      order.attachments = files;

      // Choose main attachment
      order.mainAttachment = files[0];
    }

    order.state = OrderState.PUBLISHED;

    // Send data on BE
    return orderService.create(order);
  }
}
