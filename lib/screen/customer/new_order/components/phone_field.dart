import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/bearer/user_bearer.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/screen/customer/new_order/components/field.dart';
import 'package:anyserviceui/service/validators/order_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PhoneField extends StatelessWidget {
  const PhoneField({
    Key key,
    @required this.order,
  }) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    final OrderCreationBearer bearer =
        Provider.of<OrderCreationBearer>(context, listen: false);
    final String userPhone =
        Provider.of<UserBearer>(context, listen: false).user?.phone;

    String hintValue = 'Enter your contact phone number';

    if (userPhone != null && userPhone.isNotEmpty) {
      bearer.setPhone(userPhone);
      order.phone = userPhone;
      hintValue = userPhone;
    }

    return OrderCreationField(
      hintText: hintValue,
      keyboard: TextInputType.phone,
      onChange: (String phone) {
        bearer.setPhone(phone);
        order.phone = phone;
      },
      validator: OrderValidator.validatePhoneNumber,
      key: Key(Keys.orderCreationPhoneNumber),
    );
  }
}
