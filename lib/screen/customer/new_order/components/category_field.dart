import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class CategoryField extends StatelessWidget {
  const CategoryField({
    Key key,
    this.fieldValue,
    this.textStyle,
    this.onClearButtonTap,
  }) : super(key: key);

  final String fieldValue;
  final TextStyle textStyle;
  final void Function() onClearButtonTap;

  final String emptyValue = 'Select category of an order';

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width * 0.9;

    String fieldValue;
    Color textColor;

    final String categoryTitle =
        Provider.of<OrderCreationBearer>(context).category?.title;

    if (categoryTitle == null) {
      fieldValue = emptyValue;
      textColor = kDividerColor;
    } else {
      fieldValue = categoryTitle;
      textColor = Colors.black54;
    }

    final Widget rightmostWidget = categoryTitle == null
        ? const Icon(
            FontAwesomeIcons.stream,
            size: 15.0,
            color: kDividerColor,
          )
        : GestureDetector(
            onTap: onClearButtonTap,
            child: const Icon(
              FontAwesomeIcons.timesCircle,
              size: 15.0,
            ),
          );

    return Container(
      width: width,
      decoration: Styles.getBoxDecoration(),
      padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            fieldValue,
            style: TextStyle(
              fontSize: 15.0,
              color: textColor,
            ),
          ),
          rightmostWidget,
        ],
      ),
    );
  }
}
