import 'package:anyserviceui/component/common/multiselect_field.dart';
import 'package:anyserviceui/model/dto/category.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/service/crud/category_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategorySelector extends StatelessWidget {
  const CategorySelector({
    Key key,
    @required this.order,
  }) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    final CategoryService categoryService =
        Provider.of<CategoryService>(context);

    return FutureBuilder<List<Category>>(
      future: categoryService.findAll(),
      builder: (BuildContext context, AsyncSnapshot<List<Category>> snapshot) {
        Widget result;

        if (snapshot.hasData) {
          final List<Category> categories = snapshot.data;

          final List<Map<String, String>> dataSource = categories
              .map((Category category) => <String, String>{
                    'display': category.title,
                    'id': category.id,
                  })
              .toList();

          result = MultiSelectField(
            dataSource: dataSource,
            onSaved: (dynamic value) {
              if (value == null) return;

              final List<String> selectedCategoriesIdAsList =
                  (value as List<dynamic>)
                      .map((dynamic e) => e as String)
                      .toList();

              final List<Category> selectedCategories =
                  categoryService.categoryIdListToCategories(
                categoryIdList: selectedCategoriesIdAsList,
                whereToSearch: categories,
              );

              order.categories = selectedCategories;
            },
          );
        } else if (snapshot.hasError) {
          result = const Icon(
            Icons.error_outline,
            color: Colors.red,
            size: 60,
          );
        } else {
          result = const SizedBox(
            width: 60,
            height: 60,
            child: CircularProgressIndicator(),
          );
        }

        return result;
      },
    );
  }
}
