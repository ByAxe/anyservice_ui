import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/dto/category.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/screen/customer/new_order/components/category_field.dart';
import 'package:anyserviceui/screen/customer/new_order/components/category_tree_selector.dart';
import 'package:anyserviceui/service/crud/category_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';

class CategoryPicker extends StatelessWidget {
  const CategoryPicker({
    Key key,
    @required this.order,
  }) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    final OrderCreationBearer bearer =
        Provider.of<OrderCreationBearer>(context);

    final CategoryService categoryService =
        Provider.of<CategoryService>(context);

    return FutureBuilder<List<Category>>(
      future: categoryService.findAll(),
      builder: (BuildContext context, AsyncSnapshot<List<Category>> snapshot) {
        if (snapshot.hasData && bearer.allCategories.isEmpty) {
          bearer.setAllCategories(snapshot.data);
        }

        return GestureDetector(
          onTap: () => showBarModalBottomSheet(
            expand: true,
            context: context,
            backgroundColor: Colors.transparent,
            builder: (_, __) {
              return ChangeNotifierProvider<OrderCreationBearer>.value(
                value: bearer,
                child: Material(
                  child: CupertinoPageScaffold(
                    navigationBar: CupertinoNavigationBar(
                      backgroundColor: kCustomerAccentColor,
                      leading: Container(),
                      middle: const Text(
                        'Category tree',
                        style: TextStyle(
                          color: kTextIconsColor,
                        ),
                      ),
                      trailing: IconButton(
                        icon: const Icon(
                          FontAwesomeIcons.times,
                          color: kTextIconsColor,
                        ),
                        onPressed: () {
                          clearCategories(context);
                          Navigator.pop(context);
                        },
                        key: Key(Keys.orderCreationCategoriesModalCloseButton),
                      ),
                    ),
                    child: Scaffold(
                      body: CategoryTreeSelector(order: order),
                      floatingActionButton: FloatingActionButton(
                        onPressed: () => Navigator.pop(context),
                        backgroundColor: kCustomerAccentColor,
                        key: Key(Keys.orderCreationCategoriesModalAcceptButton),
                        child: const Icon(FontAwesomeIcons.check),
                      ),
                    ),
                  ),
                ),
              );
            },
            useRootNavigator: true,
          ),
          child: CategoryField(
            onClearButtonTap: () => clearCategories(context),
          ),
        );
      },
    );
  }

  void clearCategories(BuildContext context) {
    Provider.of<OrderCreationBearer>(context, listen: false).setCategory(null);
    order.categories = <Category>[];
  }
}
