import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/model/dto/location.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/screen/customer/new_order/components/field.dart';
import 'package:anyserviceui/service/validators/order_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderCreationAddressField extends StatelessWidget {
  const OrderCreationAddressField({
    Key key,
    @required this.order,
  }) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    final bool addressRequired = LocationType.AT_CUSTOMER ==
        Provider.of<OrderCreationBearer>(context).locationType;

    return Visibility(
      visible: addressRequired,
      child: OrderCreationField(
        hintText: 'Enter your address (city, street etc.)',
        keyboard: TextInputType.streetAddress,
        onChange: (String address) => order.location.addressLine1 = address,
        validator: (String address) {
          if (!addressRequired) return null;

          return OrderValidator.validateAddress(address);
        },
      ),
    );
  }
}
