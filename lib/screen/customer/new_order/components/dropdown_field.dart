import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:anyserviceui/model/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class OrderCreationDropdownField<T> extends StatelessWidget {
  const OrderCreationDropdownField({
    Key key,
    this.elements,
    this.hintText,
    this.onConfirm,
  }) : super(key: key);

  final String hintText;
  final List<T> elements;
  final void Function(Picker, List<int>) onConfirm;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width * 0.9;

    return GestureDetector(
      onTap: () => showCountryPicker(context),
      child: Container(
        width: width,
        padding: const EdgeInsets.symmetric(
          vertical: 15.0,
          horizontal: 20.0,
        ),
        decoration: Styles.getBoxDecoration(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              hintText,
              style: kFieldTextStyle,
            ),
            Icon(
              FontAwesomeIcons.angleDown,
              color: kDividerColor,
            )
          ],
        ),
      ),
    );
  }

  void showCountryPicker(BuildContext context) {
    final Picker picker = Picker(
      adapter: PickerDataAdapter<T>(pickerdata: elements),
      changeToFirst: true,
      selectedTextStyle: const TextStyle(color: Colors.blue),
      onConfirm: onConfirm,
    );

    picker.showModal(context);
  }
}
