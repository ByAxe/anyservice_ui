import 'package:anyserviceui/bearer/assets_bearer.dart';
import 'package:anyserviceui/bearer/order_creation_bearer.dart';
import 'package:anyserviceui/component/app_bar/app_bar_mobile.dart';
import 'package:anyserviceui/component/common/basic_container.dart';
import 'package:anyserviceui/component/common/image_picker/image_picker.dart';
import 'package:anyserviceui/model/constants/application.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/screen/api/i_standalone_screen.dart';
import 'package:anyserviceui/screen/customer/new_order/components/clear_attachment_button.dart';
import 'package:anyserviceui/screen/customer/new_order/components/order_creation_button.dart';
import 'package:anyserviceui/screen/customer/new_order/components/order_creation_form.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class CustomerNewOrderScreenMobile extends StatelessWidget
    implements IStandaloneScreen {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Order _order = Order.basic();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <ChangeNotifierProvider>[
        ChangeNotifierProvider<OrderCreationBearer>(
            create: (_) => OrderCreationBearer()),
        ChangeNotifierProvider<AssetsBearer>(create: (_) => AssetsBearer()),
      ],
      child: Scaffold(
        backgroundColor: kCustomerAccentColor,
        key: _scaffoldKey,
        appBar: AppBarMobile(
          leaveBackButton: false,
          title: 'Create order',
          actions: buildActions(context),
        ),
        body: CurvedContainer(
          child: ListView(
            children: <Widget>[
              AspectRatio(
                aspectRatio: kOrderCreationScreenImagesBlockAspectRatio,
                child: ImagePicker(),
              ),
              const ClearAttachmentButton(),
              OrderCreationForm(
                formKey: _formKey,
                order: _order,
              ),
              AspectRatio(
                // Placeholder to give space at the bottom
                aspectRatio: kOrderCreationScreenBottomSpaceAspectRatio,
                child: Container(),
              ),
            ],
          ),
        ),
        floatingActionButton: OrderCreationButton(
          formKey: _formKey,
          order: _order,
          key: Key(Keys.orderCreationButton),
        ),
      ),
    );
  }

  /// Build an actions for app bar
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        iconSize: 30.0,
        padding: const EdgeInsets.only(right: 28.0),
        icon: const Icon(FontAwesomeIcons.times),
        onPressed: () => Navigator.pop(context),
      ),
    ];
  }
}
