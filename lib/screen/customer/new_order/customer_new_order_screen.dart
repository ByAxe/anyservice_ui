import 'package:anyserviceui/component/responsive/orientation_layout.dart';
import 'package:anyserviceui/component/responsive/screen_type_layout.dart';
import 'package:anyserviceui/screen/customer/new_order/customer_new_order_screen_mobile.dart';
import 'package:flutter/material.dart';

class CustomerNewOrderScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: CustomerNewOrderScreenMobile(),
      ),
    );
  }
}
