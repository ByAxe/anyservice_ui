import 'package:anyserviceui/screen/api/i_bottom_menu_screen.dart';
import 'package:flutter/material.dart';

class CustomerServicesScreenMobile extends StatelessWidget
    implements IBottomMenuScreen {
  @override
  Widget build(BuildContext context) {
    return const Text('Services');
  }
}
