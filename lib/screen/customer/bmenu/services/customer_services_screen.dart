import 'package:anyserviceui/component/responsive/orientation_layout.dart';
import 'package:anyserviceui/component/responsive/screen_type_layout.dart';
import 'package:anyserviceui/screen/customer/bmenu/services/customer_services_screen_mobile.dart';
import 'package:flutter/material.dart';

class CustomerServicesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: CustomerServicesScreenMobile(),
      ),
    );
  }
}
