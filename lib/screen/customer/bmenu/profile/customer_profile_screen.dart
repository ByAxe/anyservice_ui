import 'package:anyserviceui/component/responsive/orientation_layout.dart';
import 'package:anyserviceui/component/responsive/screen_type_layout.dart';
import 'package:anyserviceui/screen/customer/bmenu/profile/customer_profile_screen_mobile.dart';
import 'package:flutter/material.dart';

class CustomerProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: CustomerProfileScreenMobile(),
      ),
    );
  }
}
