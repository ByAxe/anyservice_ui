import 'package:anyserviceui/bearer/current_mode_bearer.dart';
import 'package:anyserviceui/model/data/data.dart';
import 'package:anyserviceui/model/dto/destination.dart';
import 'package:anyserviceui/screen/api/i_bottom_menu_screen.dart';
import 'package:anyserviceui/screen/auth/components/auth_dependent_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomerProfileScreenMobile extends StatelessWidget
    implements IBottomMenuScreen {
  const CustomerProfileScreenMobile({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        const Text('Customer Profile'),
        AuthDependentWidget(),
        // Change language button
        RaisedButton(
          onPressed: () => Provider.of<Data>(
            context,
            listen: false,
          ).changeLang('ru'),
          child: const Text('Русский'),
        ),
        Container(
          child: Text(context.watch<Data>().getData),
        ),
        RaisedButton(
          onPressed: () => Provider.of<Data>(
            context,
            listen: false,
          ).changeLang('en'),
          child: const Text('Английский'),
        ),
        Center(
          child: RaisedButton(
            onPressed: () => Provider.of<CurrentModeBearer>(
              context,
              listen: false,
            ).setCurrentMode(Mode.WORKER),
            child: const Text('Switch to Worker mode'),
          ),
        )
      ],
    );
  }
}
