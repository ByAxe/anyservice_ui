import 'package:anyserviceui/component/responsive/orientation_layout.dart';
import 'package:anyserviceui/component/responsive/screen_type_layout.dart';
import 'package:anyserviceui/screen/api/i_bottom_menu_screen.dart';
import 'package:anyserviceui/screen/customer/bmenu/my_orders/customer_my_orders_screen_mobile.dart';
import 'package:flutter/material.dart';

class CustomerMyOrdersScreen extends StatelessWidget
    implements IBottomMenuScreen {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: CustomerMyOrdersScreenMobile(),
      ),
    );
  }
}
