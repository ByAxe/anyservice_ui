import 'package:anyserviceui/component/responsive/orientation_layout.dart';
import 'package:anyserviceui/component/responsive/screen_type_layout.dart';
import 'package:anyserviceui/screen/api/i_bottom_menu_screen.dart';
import 'package:anyserviceui/screen/customer/bmenu/notifications/customer_notifications_screen_mobile.dart';
import 'package:flutter/material.dart';

class CustomerNotificationsScreen extends StatelessWidget
    implements IBottomMenuScreen {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: CustomerNotificationsScreenMobile(),
      ),
    );
  }
}
