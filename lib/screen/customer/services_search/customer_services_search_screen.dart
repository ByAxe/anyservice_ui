import 'package:anyserviceui/component/responsive/orientation_layout.dart';
import 'package:anyserviceui/component/responsive/screen_type_layout.dart';
import 'package:anyserviceui/screen/customer/services_search/customer_services_search_screen_mobile.dart';
import 'package:flutter/material.dart';

class CustomerServicesSearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: CustomerServicesSearchScreenMobile(),
      ),
    );
  }
}
