import 'package:anyserviceui/bearer/current_mode_bearer.dart';
import 'package:anyserviceui/model/dto/destination.dart';
import 'package:anyserviceui/screen/api/i_bottom_menu_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WorkerProfileScreen extends StatelessWidget implements IBottomMenuScreen {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        const Text('Worker Profile'),
        Center(
          child: RaisedButton(
            onPressed: () => Provider.of<CurrentModeBearer>(
              context,
              listen: false,
            ).setCurrentMode(Mode.CUSTOMER),
            child: const Text('Customer Mode'),
          ),
        )
      ],
    );
  }
}
