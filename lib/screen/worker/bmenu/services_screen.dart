import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/screen/api/i_bottom_menu_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WorkerOrdersScreen extends StatelessWidget implements IBottomMenuScreen {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        const Text('Orders screen'),
        IconButton(
          iconSize: 30.0,
          padding: const EdgeInsets.only(right: 28.0),
          icon: const Icon(FontAwesomeIcons.search),
          onPressed: () => Navigator.pushNamed(
            context,
            workerOrdersSearchRoute,
          ),
        ),
      ],
    );
  }
}
