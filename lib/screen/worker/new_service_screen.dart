import 'package:anyserviceui/component/app_bar/app_bar_mobile.dart';
import 'package:anyserviceui/screen/api/i_standalone_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WorkerNewServiceScreen extends StatelessWidget
    implements IStandaloneScreen {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarMobile(
        title: 'New Order',
        actions: buildActions(context),
      ),
      body: const Center(child: Text('New service creation form')),
    );
  }

  /// Build an actions for app bar
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        iconSize: 30.0,
        padding: const EdgeInsets.only(right: 28.0),
        icon: const Icon(FontAwesomeIcons.times),
        onPressed: () => Navigator.pop(context),
      ),
    ];
  }
}
