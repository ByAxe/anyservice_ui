import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class ImageFullScreen extends StatefulWidget {
  const ImageFullScreen({
    Key key,
    @required this.asset,
    @required this.animationTag,
  }) : super(key: key);

  final Asset asset;
  final String animationTag;

  @override
  _ImageFullScreenState createState() => _ImageFullScreenState();
}

class _ImageFullScreenState extends State<ImageFullScreen> {
  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }

  @override
  void dispose() {
    //SystemChrome.restoreSystemUIOverlays();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Scaffold(
        body: Center(
          child: FutureBuilder<Image>(
            future: assetThumbToImage(widget.asset),
            builder: (BuildContext context, AsyncSnapshot<Image> snapshot) {
              if (snapshot.hasData) {
                return Hero(
                  tag: widget.animationTag,
                  child: snapshot.data,
                );
              } else if (snapshot.hasError) {
                return Column(
                  children: <Widget>[
                    const Icon(
                      Icons.error_outline,
                      color: Colors.red,
                      size: 60,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Text('Error: ${snapshot.error}'),
                    )
                  ],
                );
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  Future<Image> assetThumbToImage(Asset asset) async {
    final ByteData byteData = await asset.getByteData();

    final Image image = Image.memory(byteData.buffer.asUint8List());

    return image;
  }
}
