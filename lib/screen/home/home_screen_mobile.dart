import 'package:anyserviceui/component/app_bar/app_bar_mobile.dart';
import 'package:anyserviceui/component/common/basic_container.dart';
import 'package:anyserviceui/component/floating_action_button/fab_mobile.dart';
import 'package:anyserviceui/component/navigation/navigation_bar.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/data/modes_maps.dart';
import 'package:anyserviceui/model/dto/destination.dart';
import 'package:anyserviceui/screen/api/i_standalone_screen.dart';
import 'package:anyserviceui/service/actions_service.dart';
import 'package:flutter/material.dart';

class HomeScreenMobile extends StatelessWidget implements IStandaloneScreen {
  const HomeScreenMobile({
    Key key,
    this.mode,
    this.destination,
  }) : super(key: key);

  final Mode mode;
  final Destination destination;

  @override
  Widget build(BuildContext context) {
    // Get route for floating action button
    final String route = ModesMaps.modesActionRouteMap[mode];

    // Get actions, specific for destination
    final List<Widget> actionsForDestination =
        ActionsService(destinationId: destination.id).getActions(context);

    // Build screen according to selected destination
    return Scaffold(
      backgroundColor: kCustomerAccentColor,
      appBar: AppBarMobile(
        title: destination.title,
        actions: actionsForDestination,
      ),
      body: CurvedContainer(
        child: destination.screen(context),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FABMobile(
        onClicked: () => Navigator.pushNamed(context, route),
      ),
      bottomNavigationBar: NavigationBar(
        mode: mode,
        selectedItemColor: kCustomerAccentColor,
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) =>
      throw UnsupportedError('Not supported');
}
