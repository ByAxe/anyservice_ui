import 'package:anyserviceui/bearer/current_mode_bearer.dart';
import 'package:anyserviceui/bearer/navigation_destination_bearer.dart';
import 'package:anyserviceui/component/responsive/orientation_layout.dart';
import 'package:anyserviceui/component/responsive/screen_type_layout.dart';
import 'package:anyserviceui/model/data/destination_list.dart';
import 'package:anyserviceui/model/dto/destination.dart';
import 'package:anyserviceui/screen/home/home_screen_mobile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Get current mode of application
    final Mode currentMode =
        Provider.of<CurrentModeBearer>(context).currentMode;

    // Get selected destination index
    final int selectedDestination =
        Provider.of<NavigationDestinationBearer>(context).selectedDestination;

    // Get selected destination
    final Destination destination = DestinationList()
        .getDestinations()
        .where((Destination d) => d.modes.contains(currentMode))
        .toList()[selectedDestination];

    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: HomeScreenMobile(mode: currentMode, destination: destination),
      ),
    );
  }
}
