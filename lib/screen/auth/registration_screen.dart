import 'package:anyserviceui/component/common/basic_back_button.dart';
import 'package:anyserviceui/screen/auth/components/colorful_container.dart';
import 'package:anyserviceui/screen/auth/components/flexible_space.dart';
import 'package:anyserviceui/screen/auth/components/h1_text.dart';
import 'package:anyserviceui/screen/auth/components/h3_text.dart';
import 'package:anyserviceui/screen/auth/components/login_text.dart';
import 'package:anyserviceui/screen/auth/components/registration_form.dart';
import 'package:anyserviceui/screen/auth/components/skip_and_continue_as_anonymous_button.dart';
import 'package:anyserviceui/screen/auth/components/sso_block.dart';
import 'package:flutter/material.dart';

class RegistrationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ColorfulContainer(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const <Widget>[
            BasicBackButton(),
            SkipAndContinueAsAnonymousButton(),
          ],
        ),
        const FlexibleSpace(aspectRatio: 15 / 1),
        const H1Text(text: 'First time here?'),
        const H3Text(text: 'Create your account in anyservice'),
        const FlexibleSpace(aspectRatio: 15 / 1),
        RegistrationForm(),
        const FlexibleSpace(aspectRatio: 30 / 1),
        const SSOBlock(),
        const FlexibleSpace(aspectRatio: 10 / 1),
        const LoginText(),
      ],
    );
  }
}
