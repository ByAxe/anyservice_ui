import 'package:anyserviceui/component/common/basic_back_button.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:anyserviceui/screen/auth/components/anonymous_login_text.dart';
import 'package:anyserviceui/screen/auth/components/colorful_container.dart';
import 'package:anyserviceui/screen/auth/components/flexible_space.dart';
import 'package:anyserviceui/screen/auth/components/h1_text.dart';
import 'package:anyserviceui/screen/auth/components/h3_text.dart';
import 'package:anyserviceui/screen/auth/components/login_form.dart';
import 'package:anyserviceui/screen/auth/components/registration_text.dart';
import 'package:anyserviceui/screen/auth/components/sso_block.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ColorfulContainer(
      children: <Widget>[
        const BasicBackButton(),
        const FlexibleSpace(aspectRatio: 15 / 1),
        const H1Text(text: 'Welcome back!'),
        const H3Text(text: 'Log in to your existing account'),
        const FlexibleSpace(),
        LoginForm(),
        const FlexibleSpace(aspectRatio: 30 / 1),
        const SSOBlock(),
        const FlexibleSpace(aspectRatio: 10 / 1),
        const RegistrationText(),
        const FlexibleSpace(
          aspectRatio: 15 / 1,
          child: Text(
            'or',
            textAlign: TextAlign.center,
            style: kH4TextStyleNotImportant,
          ),
        ),
        const AnonymousLoginText(),
      ],
    );
  }
}
