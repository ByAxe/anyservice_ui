import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:anyserviceui/service/authentication_helper.dart';
import 'package:flutter/material.dart';

class SkipAndContinueAsAnonymousButton extends StatelessWidget {
  const SkipAndContinueAsAnonymousButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => AuthHelper.signInAnonymously(context),
      child: const Text(
        'Skip',
        style: kH3TextStyleLink,
      ),
    );
  }
}
