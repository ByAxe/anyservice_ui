import 'package:anyserviceui/component/common/rounded_button.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/dto/user.dart';
import 'package:anyserviceui/screen/auth/components/auth_input_field.dart';
import 'package:anyserviceui/screen/auth/components/flexible_space.dart';
import 'package:anyserviceui/service/authentication_helper.dart';
import 'package:anyserviceui/service/validators/registration_validator.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RegistrationForm extends StatefulWidget {
  @override
  RegistrationFormState createState() {
    return RegistrationFormState();
  }
}

class RegistrationFormState extends State<RegistrationForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  User user = User.basic();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          AuthInputField(
            key: Key(Keys.registrationName),
            onChange: (String v) => user.name = v,
            validator: RegistrationValidator.validateName,
            hintText: 'Name',
            icon: FontAwesomeIcons.user,
            keyboard: TextInputType.name,
          ),
          const FlexibleSpace(aspectRatio: 20 / 1),
          AuthInputField(
            key: Key(Keys.registrationEmail),
            onChange: (String v) => user.email = v,
            validator: RegistrationValidator.validateEmail,
            hintText: 'Email',
            icon: FontAwesomeIcons.envelope,
            keyboard: TextInputType.emailAddress,
          ),
          const FlexibleSpace(aspectRatio: 20 / 1),
          AuthInputField(
            key: Key(Keys.registrationPhone),
            onChange: (String v) => user.setPhone(v),
            validator: RegistrationValidator.validatePhone,
            hintText: 'Phone',
            icon: FontAwesomeIcons.phone,
            keyboard: TextInputType.phone,
          ),
          const FlexibleSpace(aspectRatio: 20 / 1),
          AuthInputField(
            key: Key(Keys.registrationPassword),
            onChange: (String v) => user.password = v,
            validator: RegistrationValidator.validatePassword,
            hintText: 'Password',
            icon: FontAwesomeIcons.lock,
            obscureText: true,
          ),
          const FlexibleSpace(aspectRatio: 20 / 1),
          RoundedButton(
            key: Key(Keys.registrationRegisterButton),
            color: kCustomerAccentColor2,
            onPressed: () => AuthHelper.registerUser(context, _formKey, user),
            text: 'Register',
          ),
        ],
      ),
    );
  }
}
