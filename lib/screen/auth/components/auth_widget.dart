import 'dart:developer' as developer;

import 'package:anyserviceui/model/constants/logging.dart';
import 'package:anyserviceui/model/dto/user.dart';
import 'package:anyserviceui/screen/auth/auth_screen.dart';
import 'package:anyserviceui/screen/home/home_screen.dart';
import 'package:flutter/material.dart';

class AuthWidget extends StatelessWidget {
  const AuthWidget({Key key, @required this.userSnapshot}) : super(key: key);
  final AsyncSnapshot<User> userSnapshot;

  @override
  Widget build(BuildContext context) {
    developer.log('AuthWidget rebuild', name: kStateManagementLog);

    if (userSnapshot.connectionState == ConnectionState.active) {
      if (userSnapshot.hasData) {
        return HomeScreen();
      } else {
        return AuthScreen();
      }
    } else {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
  }
}
