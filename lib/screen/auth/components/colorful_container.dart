import 'package:anyserviceui/bearer/spinner_state_bearer.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

class ColorfulContainer extends StatelessWidget {
  const ColorfulContainer({
    Key key,
    this.children,
    this.padding = const EdgeInsets.symmetric(horizontal: 24.0),
  }) : super(key: key);

  final List<Widget> children;
  final EdgeInsets padding;

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: Provider.of<SpinnerStateBearer>(context).isShowSpinner,
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.longestSide,
          decoration: kAppGradient,
          child: Padding(
            padding: padding,
            child: ListView(
              children: children,
            ),
          ),
        ),
      ),
    );
  }
}
