import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:anyserviceui/service/authentication_helper.dart';
import 'package:flutter/material.dart';

class AnonymousLoginText extends StatelessWidget {
  const AnonymousLoginText({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text(
          'Try our application as ',
          style: kH4TextStyle,
        ),
        GestureDetector(
          onTap: () => AuthHelper.signInAnonymously(context),
          key: Key(Keys.anonymousLoginTextButton),
          child: const Text(
            'Anonymous User',
            style: kH4TextStyleLink,
          ),
        )
      ],
    );
  }
}
