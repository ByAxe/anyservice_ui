import 'package:anyserviceui/component/common/rounded_button.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/screen/auth/components/auth_input_field.dart';
import 'package:anyserviceui/screen/auth/components/flexible_space.dart';
import 'package:anyserviceui/screen/auth/components/forgot_password.dart';
import 'package:anyserviceui/service/authentication_helper.dart';
import 'package:anyserviceui/service/validators/login_validator.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginForm extends StatefulWidget {
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String email;
  String password;

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          AuthInputField(
            key: Key(Keys.loginEmail),
            onChange: (String v) => email = v,
            validator: LoginValidator.validateEmail,
            hintText: 'Email',
            icon: FontAwesomeIcons.envelope,
            keyboard: TextInputType.emailAddress,
          ),
          const FlexibleSpace(aspectRatio: 20 / 1),
          AuthInputField(
            key: Key(Keys.loginPassword),
            onChange: (String v) => password = v,
            validator: LoginValidator.validatePassword,
            hintText: 'Password',
            icon: FontAwesomeIcons.lock,
            obscureText: true,
          ),
          const FlexibleSpace(aspectRatio: 20 / 1),
          const ForgotPassword(),
          const FlexibleSpace(aspectRatio: 30 / 1),
          RoundedButton(
            key: Key(Keys.loginButton),
            onPressed: () =>
                AuthHelper.signIn(context, _formKey, email, password),
            text: 'Login',
            color: kCustomerAccentColor2,
          ),
        ],
      ),
    );
  }
}
