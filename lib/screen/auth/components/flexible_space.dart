import 'package:flutter/material.dart';

class FlexibleSpace extends StatelessWidget {
  const FlexibleSpace({
    Key key,
    this.aspectRatio = 5 / 1,
    this.child,
  }) : super(key: key);

  final double aspectRatio;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: aspectRatio,
      child: child ?? Container(),
    );
  }
}
