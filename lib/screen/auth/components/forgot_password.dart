import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/material.dart';

class ForgotPassword extends StatelessWidget {
  const ForgotPassword({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: const Text(
        'Forgot password?',
        textAlign: TextAlign.end,
        style: kH3TextStyleImportant,
      ),
    );
  }
}
