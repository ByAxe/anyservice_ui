import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/material.dart';

class LoginText extends StatelessWidget {
  const LoginText({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text(
          'Already have an account? ',
          style: kH4TextStyle,
        ),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, loginRoute),
          key: Key(Keys.loginTextButton),
          child: const Text(
            'Login here',
            style: kH4TextStyleLink,
          ),
        )
      ],
    );
  }
}
