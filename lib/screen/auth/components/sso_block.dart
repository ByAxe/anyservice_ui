import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:anyserviceui/screen/auth/components/sso_row.dart';
import 'package:flutter/material.dart';

class SSOBlock extends StatelessWidget {
  const SSOBlock({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const <Widget>[
        Text(
          'Or connect using',
          textAlign: TextAlign.center,
          style: kH4TextStyleNotImportant,
        ),
        SSORow(),
      ],
    );
  }
}
