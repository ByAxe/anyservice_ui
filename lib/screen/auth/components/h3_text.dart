import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/material.dart';

class H3Text extends StatelessWidget {
  const H3Text({
    Key key,
    this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: kH3TextStyle,
    );
  }
}
