import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/routes.dart';
import 'package:flutter/material.dart';

class AuthorizeButton extends StatelessWidget {
  const AuthorizeButton({
    Key key,
    @required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RaisedButton(
            onPressed: () => Navigator.pushNamed(context, authRoute),
            padding: const EdgeInsets.all(10.0),
            color: kTextIconsColor,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Text(
              text,
              style: const TextStyle(
                color: kPrimaryTextColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
