import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SSORow extends StatelessWidget {
  const SSORow({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SSOItem(onTap: () {}, icon: FontAwesomeIcons.google),
        SSOItem(onTap: () {}, icon: FontAwesomeIcons.facebook),
        SSOItem(onTap: () {}, icon: FontAwesomeIcons.apple),
        SSOItem(onTap: () {}, icon: FontAwesomeIcons.twitter),
        SSOItem(onTap: () {}, icon: FontAwesomeIcons.github),
        SSOItem(onTap: () {}, icon: FontAwesomeIcons.microsoft),
      ],
    );
  }
}

class SSOItem extends StatelessWidget {
  const SSOItem({
    Key key,
    this.onTap,
    this.icon,
  }) : super(key: key);

  final void Function() onTap;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 10.0,
          horizontal: 8.0,
        ),
        child: Icon(
          icon,
          color: kTextIconsColor,
          size: 30.0,
        ),
      ),
    );
  }
}
