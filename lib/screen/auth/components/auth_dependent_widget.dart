import 'package:anyserviceui/bearer/user_bearer.dart';
import 'package:anyserviceui/component/common/avatar.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/dto/user.dart';
import 'package:anyserviceui/screen/auth/components/authorize_button.dart';
import 'package:anyserviceui/service/firebase_auth_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthDependentWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final FirebaseAuthService auth = Provider.of<FirebaseAuthService>(context);

    if (auth.isUserLoggedIn) {
      final User user = Provider.of<UserBearer>(context).user;

      return GestureDetector(
        key: Key(Keys.logoutButton),
        onTap: () => auth.signOut(),
        child: Avatar(
          photoUrl: user.firebaseUser.photoUrl,
          radius: 22,
        ),
      );
    } else {
      return AuthorizeButton(
        text: 'Авторизоваться',
        key: Key(Keys.authButton),
      );
    }
  }
}
