import 'package:anyserviceui/component/common/rounded_text_field.dart';
import 'package:anyserviceui/model/constants/styles.dart';
import 'package:flutter/material.dart';

class AuthInputField extends StatelessWidget {
  const AuthInputField({
    Key key,
    this.onChange,
    this.validator,
    this.textAlign = TextAlign.start,
    this.hintText,
    this.icon,
    this.obscureText = false,
    this.keyboard,
  }) : super(key: key);

  final void Function(String) onChange;
  final String Function(String) validator;
  final TextAlign textAlign;
  final String hintText;
  final IconData icon;
  final bool obscureText;
  final TextInputType keyboard;

  @override
  Widget build(BuildContext context) {
    return RoundedTextField(
      onChange: onChange,
      textAlign: textAlign,
      validator: validator,
      inputDecoration: Styles.getAuthInputDecoration(
        hintText: hintText,
        prefixIcon: icon,
      ),
      obscureText: obscureText,
      keyboard: keyboard,
    );
  }
}
