import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/material.dart';

class RegistrationText extends StatelessWidget {
  const RegistrationText({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text(
          "Don't have an account yet? ",
          style: kH4TextStyle,
        ),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, registrationRoute),
          key: Key(Keys.registrationTextButton),
          child: const Text(
            'Registration',
            style: kH4TextStyleLink,
          ),
        )
      ],
    );
  }
}
