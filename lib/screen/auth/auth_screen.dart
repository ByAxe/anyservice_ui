import 'package:anyserviceui/component/common/rounded_button.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/screen/auth/components/colorful_container.dart';
import 'package:anyserviceui/screen/auth/components/flexible_space.dart';
import 'package:anyserviceui/screen/auth/components/h1_text.dart';
import 'package:anyserviceui/screen/auth/components/registration_text.dart';
import 'package:anyserviceui/screen/auth/components/sso_block.dart';
import 'package:anyserviceui/service/authentication_helper.dart';
import 'package:flutter/material.dart';

class AuthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ColorfulContainer(
      children: <Widget>[
        const FlexibleSpace(aspectRatio: 5 / 2),
        const H1Text(text: 'Welcome to AnyService!'),
        const FlexibleSpace(),
        RoundedButton(
          key: Key(Keys.authLoginButton),
          text: 'Log in',
          onPressed: () => Navigator.pushNamed(context, loginRoute),
          color: kCustomerAccentColor0,
        ),
        RoundedButton(
          key: Key(Keys.anonymousLoginButton),
          text: 'Anonymous login',
          onPressed: () => AuthHelper.signInAnonymously(context),
          color: kCustomerAccentColor0,
        ),
        const FlexibleSpace(aspectRatio: 10 / 1),
        const SSOBlock(),
        const FlexibleSpace(aspectRatio: 10 / 1),
        const RegistrationText(),
      ],
    );
  }
}
