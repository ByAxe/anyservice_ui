import 'package:flutter/material.dart';

/// This abstract class defines all common methods
/// for all stand alone screens in application
abstract class IStandaloneScreen {
  /// Build an actions for app bar
  List<Widget> buildActions(BuildContext context);
}
