import 'package:anyserviceui/component/app_bar/app_bar_mobile.dart';
import 'package:anyserviceui/component/common/basic_container.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/screen/api/i_standalone_screen.dart';
import 'package:anyserviceui/screen/auth/components/auth_dependent_widget.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SettingsScreenMobile extends StatelessWidget
    implements IStandaloneScreen {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kCustomerAccentColor,
      appBar: AppBarMobile(
        title: 'Settings',
        actions: buildActions(context),
      ),
      body: CurvedContainer(
        child: Column(
          children: <Widget>[
            AuthDependentWidget(),
          ],
        ),
      ),
    );
  }

  /// Build an actions for app bar
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        iconSize: 30.0,
        padding: const EdgeInsets.only(right: 28.0),
        icon: const Icon(FontAwesomeIcons.times),
        onPressed: () => Navigator.pop(context),
        key: Key(Keys.settingsCloseButton),
      ),
    ];
  }
}
