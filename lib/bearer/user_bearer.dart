import 'package:anyserviceui/model/dto/user.dart';

class UserBearer {
  const UserBearer(this.user);

  final User user;
}
