import 'dart:collection';

import 'package:anyserviceui/model/dto/category.dart';
import 'package:anyserviceui/model/dto/location.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:flutter/material.dart';

class OrderCreationBearer extends ChangeNotifier {
  DateTime _dateTime;
  LocationType _locationType;
  bool _showSpinner = false;
  bool _isDiscussablePrice = false;
  String _price;
  Currency _currency = Currency.EUR;
  Category _category;
  String _phone;
  final TextEditingController _priceController = TextEditingController();
  final List<Category> _allCategories = <Category>[];

  DateTime get dateTime => _dateTime;

  LocationType get locationType => _locationType;

  bool get showSpinner => _showSpinner;

  bool get isDiscussablePrice => _isDiscussablePrice;

  String get price => _price;

  String get phone => _phone;

  Currency get currency => _currency;

  Category get category => _category;

  TextEditingController get priceController => _priceController;

  List<Category> get allCategories =>
      UnmodifiableListView<Category>(_allCategories);

  void setPhone(String phone) {
    _phone = phone;
    notifyListeners();
  }

  void setDeadline(DateTime dateTime) {
    _dateTime = dateTime;
    notifyListeners();
  }

  void setLocationType(LocationType locationType) {
    _locationType = locationType;
    notifyListeners();
  }

  void toggleSpinner() {
    // Toggle spinner value
    _showSpinner = !_showSpinner;
    notifyListeners();
  }

  void setDiscussablePrice({bool isDiscussablePrice}) {
    _isDiscussablePrice = isDiscussablePrice;
    notifyListeners();
  }

  void setPrice(String price) {
    _price = price;
    notifyListeners();
  }

  void setCurrency(Currency currency) {
    _currency = currency;
    notifyListeners();
  }

  void setCategory(Category category) {
    _category = category;
    notifyListeners();
  }

  void setAllCategories(List<Category> categories) {
    _allCategories.clear();
    _allCategories.addAll(categories);
    // notifyListeners();
  }
}
