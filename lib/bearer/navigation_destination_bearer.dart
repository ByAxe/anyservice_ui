import 'package:flutter/material.dart';

/// This service stores data about currently selected page
/// of bottom navigation bar
class NavigationDestinationBearer extends ChangeNotifier {
  int _selectedDestination = 0;

  int get selectedDestination => _selectedDestination;

  void setDestination(int newValue) {
    _selectedDestination = newValue;
    notifyListeners();
  }
}
