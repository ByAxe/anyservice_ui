import 'package:anyserviceui/model/dto/destination.dart';
import 'package:flutter/material.dart';

/// Stores current mode of application (Customer, Worker etc.)
///
/// By default turned on CUSTOMER mode
class CurrentModeBearer extends ChangeNotifier {
  Mode _currentMode = Mode.CUSTOMER;

  Mode get currentMode => _currentMode;

  void setCurrentMode(Mode mode) {
    _currentMode = mode;
    notifyListeners();
  }
}
