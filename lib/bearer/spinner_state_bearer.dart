import 'package:flutter/material.dart';

/// Stores current state of a pre-loader\spinner\wheel in application
class SpinnerStateBearer extends ChangeNotifier {
  bool _showSpinner = false;

  bool get isShowSpinner => _showSpinner;

  void showSpinner() {
    _showSpinner = true;
    notifyListeners();
  }

  void hideSpinner() {
    _showSpinner = false;
    notifyListeners();
  }

  void toggleSpinner() {
    _showSpinner = !_showSpinner;
    notifyListeners();
  }
}
