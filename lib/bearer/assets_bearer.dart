import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

/// Stores current mode of application (Customer, Worker etc.)
///
/// By default turned on CUSTOMER mode
class AssetsBearer extends ChangeNotifier {
  List<Asset> _assets = <Asset>[];

  List<Asset> get assets => _assets;

  void setAssets(List<Asset> assets) {
    _assets = assets;
    notifyListeners();
  }

  void addAssets(List<Asset> assets) {
    _assets.addAll(assets);

    notifyListeners();
  }

  void cleanAssets() {
    setAssets(<Asset>[]);
  }

  Asset removeByIndex(int index) {
    final Asset removed = _assets.removeAt(index);

    notifyListeners();

    return removed;
  }
}
