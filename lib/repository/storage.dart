import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class MySecureStorage {
  final FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<void> save(String key, String value) async {
    storage.write(key: key, value: value);
  }

  Future<String> read(String key) async {
    return storage.read(key: key);
  }
}
