import 'package:anyserviceui/auth_widget_builder.dart';
import 'package:anyserviceui/bearer/current_mode_bearer.dart';
import 'package:anyserviceui/bearer/navigation_destination_bearer.dart';
import 'package:anyserviceui/bearer/spinner_state_bearer.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/model/data/data.dart';
import 'package:anyserviceui/model/dto/user.dart';
import 'package:anyserviceui/screen/auth/auth_screen.dart';
import 'package:anyserviceui/screen/auth/components/auth_widget.dart';
import 'package:anyserviceui/screen/auth/login_screen.dart';
import 'package:anyserviceui/screen/auth/registration_screen.dart';
import 'package:anyserviceui/screen/customer/new_order/customer_new_order_screen.dart';
import 'package:anyserviceui/screen/customer/services_search/customer_services_search_screen.dart';
import 'package:anyserviceui/screen/settings/settings_screen.dart';
import 'package:anyserviceui/screen/worker/new_service_screen.dart';
import 'package:anyserviceui/screen/worker/orders_search_screen.dart';
import 'package:anyserviceui/service/firebase_auth_service.dart';
import 'package:anyserviceui/service/utility/alert_service.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        Provider<FirebaseAuthService>(create: (_) => FirebaseAuthService()),
        ChangeNotifierProvider<SpinnerStateBearer>(
            create: (_) => SpinnerStateBearer()),
        ChangeNotifierProvider<CurrentModeBearer>(
            create: (_) => CurrentModeBearer()),
        ChangeNotifierProvider<NavigationDestinationBearer>(
            create: (_) => NavigationDestinationBearer()),
        Provider<AlertService>(create: (_) => AlertService()),
        ChangeNotifierProvider<Data>(create: (BuildContext context) => Data()),
      ],
      child: AuthWidgetBuilder(
        builder: (BuildContext context, AsyncSnapshot<User> userSnapshot) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'AnyService',
            theme: ThemeData(
              primaryColor: kPrimaryColor,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              textTheme: GoogleFonts.latoTextTheme(
                Theme.of(context).textTheme,
              ),
            ),
            home: AuthWidget(userSnapshot: userSnapshot),
            routes: <String, Widget Function(BuildContext)>{
              authRoute: (BuildContext context) => AuthScreen(),
              loginRoute: (BuildContext context) => LoginScreen(),
              registrationRoute: (BuildContext context) => RegistrationScreen(),
              customerNewOrderRoute: (BuildContext context) =>
                  CustomerNewOrderScreen(),
              customerServicesSearchRoute: (BuildContext context) =>
                  CustomerServicesSearchScreen(),
              workerNewServiceRoute: (BuildContext context) =>
                  WorkerNewServiceScreen(),
              workerOrdersSearchRoute: (BuildContext context) =>
                  WorkerOrdersSearchScreen(),
              settingsRoute: (BuildContext context) => SettingsScreen(),
            },
          );
        },
      ),
    );
  }
}
