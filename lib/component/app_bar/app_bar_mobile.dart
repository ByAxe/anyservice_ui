import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/material.dart';

class AppBarMobile extends StatefulWidget implements PreferredSizeWidget {
  const AppBarMobile({
    Key key,
    @required this.title,
    @required this.actions,
    this.leaveBackButton = true,
  })  : preferredSize = const Size.fromHeight(65.0),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0
  final String title;
  final List<Widget> actions;
  final bool leaveBackButton;

  @override
  _AppBarMobileState createState() => _AppBarMobileState();
}

class _AppBarMobileState extends State<AppBarMobile> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: kCustomerAccentColor,
      automaticallyImplyLeading: widget.leaveBackButton,
      title: Row(
        children: <Widget>[
          const SizedBox(width: 20.0),
          Text(
            widget.title,
            style: const TextStyle(
              color: kTextIconsColor,
              fontSize: 30.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
      elevation: 0.0,
      titleSpacing: 0.0,
      actions: widget.actions,
    );
  }
}
