import 'package:anyserviceui/core/ui/device_screen_type.dart';
import 'package:flutter/material.dart';

class SizingInformation {
  const SizingInformation(
      {this.deviceScreenType, this.screenSize, this.localWidgetSize});

  final DeviceScreenType deviceScreenType;
  final Size screenSize;
  final Size localWidgetSize;

  @override
  String toString() {
    return 'DeviceType:$deviceScreenType '
        'ScreenSize:$screenSize LocalWidgetSize:$localWidgetSize';
  }
}
