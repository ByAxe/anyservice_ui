import 'package:anyserviceui/component/responsive/sizing_information.dart';
import 'package:anyserviceui/core/ui/ui_utils.dart';
import 'package:flutter/material.dart';

class ResponsiveBuilder extends StatelessWidget {
  const ResponsiveBuilder({Key key, this.builder}) : super(key: key);

  final Widget Function(
    BuildContext context,
    SizingInformation sizinginformation,
  ) builder;

  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints boxConstraints) => builder(
        context,
        SizingInformation(
          deviceScreenType: getDeviceScreenType(mediaQuery),
          screenSize: mediaQuery.size,
          localWidgetSize: Size(
            boxConstraints.maxWidth,
            boxConstraints.maxHeight,
          ),
        ),
      ),
    );
  }
}
