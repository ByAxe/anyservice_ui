import 'package:flutter/material.dart';

class OrientationLayout extends StatelessWidget {
  const OrientationLayout({
    Key key,
    this.portrait,
    this.landscape,
  }) : super(key: key);

  final Widget portrait;
  final Widget landscape;

  @override
  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    // If orientation is landscape and we have special widget for it - use it
    if (orientation == Orientation.landscape) {
      return landscape ?? portrait;
    }

    // By default return portrait widgets
    return portrait;
  }
}
