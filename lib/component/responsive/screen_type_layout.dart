import 'package:anyserviceui/component/responsive/responsive_builder.dart';
import 'package:anyserviceui/component/responsive/sizing_information.dart';
import 'package:anyserviceui/core/ui/device_screen_type.dart';
import 'package:flutter/material.dart';

class ScreenTypeLayout extends StatelessWidget {
  const ScreenTypeLayout({
    Key key,
    this.mobile,
    this.tablet,
    this.desktop,
  }) : super(key: key);

  final Widget mobile;
  final Widget tablet;
  final Widget desktop;

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (
        BuildContext context,
        SizingInformation sizingInformation,
      ) {
        // If it's a tablet and we have specific widget for it - use it
        if (sizingInformation.deviceScreenType == DeviceScreenType.TABLET &&
            tablet != null) {
          return tablet;
        }

        // If it's a desktop and we have specific widget for it - use it
        if (sizingInformation.deviceScreenType == DeviceScreenType.DESKTOP &&
            desktop != null) {
          return desktop;
        }

        // Or by default use mobile widget layout
        return mobile;
      },
    );
  }
}
