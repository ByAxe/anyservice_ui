import 'package:anyserviceui/bearer/navigation_destination_bearer.dart';
import 'package:anyserviceui/model/data/destination_list.dart';
import 'package:anyserviceui/model/dto/destination.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Bottom navigation bar in application
class NavigationBar extends StatelessWidget {
  const NavigationBar({
    Key key,
    @required this.mode,
    @required this.selectedItemColor,
  }) : super(key: key);

  final Mode mode;
  final Color selectedItemColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(color: Colors.black12, blurRadius: 15),
        ],
      ),
      child: BottomNavigationBar(
        items: DestinationList()
            .getDestinations()
            .where((Destination d) => d.modes.contains(mode))
            .map(
              (Destination d) => BottomNavigationBarItem(
                icon: Icon(d.icon),
                title: Text(d.title),
              ),
            )
            .toList(),
        iconSize: 18.0,
        currentIndex: Provider.of<NavigationDestinationBearer>(context)
            .selectedDestination,
        selectedItemColor: selectedItemColor,
        unselectedItemColor: Colors.black26,
        showUnselectedLabels: true,
        onTap: (int index) => Provider.of<NavigationDestinationBearer>(
          context,
          listen: false,
        ).setDestination(index),
      ),
    );
  }
}
