import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/material.dart';

class FABMobile extends FloatingActionButton {
  const FABMobile({this.onClicked});

  final VoidCallback onClicked;

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onClicked,
      key: Key(Keys.fab),
      backgroundColor: kCustomerAccentColor,
      child: const Icon(
        Icons.add,
        size: 30,
      ),
    );
  }
}
