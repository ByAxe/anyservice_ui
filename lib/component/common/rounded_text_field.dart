import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/material.dart';

class RoundedTextField extends StatelessWidget {
  const RoundedTextField({
    Key key,
    @required this.onChange,
    this.validator,
    this.hintText = '',
    this.obscureText = false,
    this.keyboard = TextInputType.text,
    this.width,
    this.inputDecoration,
    this.boxDecoration,
    this.margin,
    this.textAlign = TextAlign.center,
    this.textStyle = const TextStyle(color: kTextIconsColor),
    this.isEnabled = true,
    this.controller,
    this.minLines = 1,
    this.maxLines = 1,
  }) : super(key: key);

  final void Function(String) onChange;
  final String Function(String) validator;
  final String hintText;
  final bool obscureText;
  final TextInputType keyboard;
  final double width;
  final EdgeInsetsGeometry margin;
  final InputDecoration inputDecoration;
  final BoxDecoration boxDecoration;
  final TextStyle textStyle;
  final TextAlign textAlign;
  final bool isEnabled;
  final TextEditingController controller;
  final int minLines;
  final int maxLines;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: boxDecoration,
      margin: margin,
      width: width,
      child: TextFormField(
        minLines: minLines,
        maxLines: maxLines,
        controller: controller,
        enabled: isEnabled,
        style: textStyle,
        obscureText: obscureText,
        textAlign: textAlign,
        keyboardType: keyboard,
        onChanged: onChange,
        decoration:
            inputDecoration ?? kInputDecoration.copyWith(hintText: hintText),
        validator: validator,
      ),
    );
  }
}
