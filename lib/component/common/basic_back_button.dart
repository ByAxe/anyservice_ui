import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/material.dart';

class BasicBackButton extends StatelessWidget {
  const BasicBackButton({
    Key key,
    this.onPressed,
  }) : super(key: key);

  final void Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        BackButton(
          onPressed: onPressed,
          color: kTextIconsColor,
        ),
      ],
    );
  }
}
