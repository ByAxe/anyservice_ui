import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  const RoundedButton({
    Key key,
    this.onPressed,
    this.text,
    this.color,
    this.padding = const EdgeInsets.symmetric(vertical: 16.0),
    this.minWidth = 200.0,
    this.height = 42.0,
    this.textStyle = kButtonsTextStyle,
  }) : super(key: key);

  final void Function() onPressed;
  final String text;
  final Color color;
  final EdgeInsets padding;
  final double minWidth;
  final double height;
  final TextStyle textStyle;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Material(
        elevation: 5.0,
        color: color,
        borderRadius: BorderRadius.circular(30.0),
        child: MaterialButton(
          onPressed: onPressed,
          minWidth: minWidth,
          height: height,
          child: Text(
            text,
            style: textStyle,
          ),
        ),
      ),
    );
  }
}
