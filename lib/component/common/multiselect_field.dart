import 'package:anyserviceui/model/constants/styles.dart';
import 'package:anyserviceui/service/validators/order_validator.dart';
import 'package:flutter/material.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';

class MultiSelectField extends StatelessWidget {
  const MultiSelectField({
    Key key,
    this.dataSource,
    this.onSaved,
  }) : super(key: key);

  final List<Map<String, String>> dataSource;
  final void Function(dynamic value) onSaved;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width * 0.9;

    return Container(
      decoration: Styles.getBoxDecoration(),
      width: width,
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: MultiSelectFormField(
        titleText: 'Select categories of an order',
        fillColor: Colors.transparent,
        validator: OrderValidator.validateCategorySelection,

        // Category: "display": title, "id":id
        dataSource: dataSource,
        border: InputBorder.none,
        okButtonLabel: 'OK',
        cancelButtonLabel: 'CANCEL',
        textField: 'display',
        valueField: 'id',
        required: true,
        hintText: 'Please choose one or more',
        onSaved: onSaved,
      ),
    );
  }
}
