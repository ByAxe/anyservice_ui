import 'package:anyserviceui/core/utils.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DateTimePicker extends StatelessWidget {
  const DateTimePicker({
    Key key,
    this.fieldValue,
    this.textStyle,
    this.onClearButtonTap,
    this.onConfirm,
    this.minValue,
    this.maxValue,
  }) : super(key: key);

  final DateTime fieldValue;
  final DateTime minValue;
  final DateTime maxValue;
  final TextStyle textStyle;
  final String emptyValue = 'If an order has deadline, click here';
  final void Function() onClearButtonTap;
  final void Function(Picker picker, List<int> value) onConfirm;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width * 0.9;

    final String text = fieldValue != null
        ? 'Deadline is ${convertDateTimeToString(fieldValue)}'
        : emptyValue;

    final Widget rightmostWidget = fieldValue == null
        ? const Icon(
            FontAwesomeIcons.calendar,
            size: 15.0,
            color: kDividerColor,
          )
        : GestureDetector(
            onTap: onClearButtonTap,
            child: const Icon(
              FontAwesomeIcons.timesCircle,
              size: 15.0,
            ),
          );

    return GestureDetector(
      onTap: () => showPickerDate(context),
      child: Container(
        decoration: Styles.getBoxDecoration(),
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
        margin: const EdgeInsets.symmetric(vertical: 10.0),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              text,
              style: textStyle,
            ),
            rightmostWidget,
          ],
        ),
      ),
    );
  }

  void showPickerDate(BuildContext context) {
    final Picker picker = Picker(
      hideHeader: true,
      adapter: DateTimePickerAdapter(
        minValue: minValue,
        maxValue: maxValue,
      ),
      title: const Text('Select Deadline'),
      selectedTextStyle: const TextStyle(color: Colors.blue),
      onConfirm: onConfirm,
    );

    picker.showDialog(context);
  }
}
