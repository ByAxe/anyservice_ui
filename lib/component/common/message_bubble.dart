import 'package:flutter/material.dart';

const Radius kMessageBubbleBorderRadius = Radius.circular(50.0);

class MessageBubble extends StatelessWidget {
  const MessageBubble(
      {Key key, this.text, this.sender, this.isMe = true, this.dtCreate})
      : super(key: key);

  final String text;
  final String sender;
  final DateTime dtCreate;
  final bool isMe;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Text(
            sender,
            style: const TextStyle(
              fontSize: 12.0,
              color: Colors.black54,
            ),
          ),
          Material(
            elevation: 5.0,
            borderRadius: BorderRadius.only(
              topLeft: isMe ? kMessageBubbleBorderRadius : Radius.zero,
              topRight: isMe ? Radius.zero : kMessageBubbleBorderRadius,
              bottomLeft: kMessageBubbleBorderRadius,
              bottomRight: kMessageBubbleBorderRadius,
            ),
            color: isMe ? Colors.lightBlueAccent : Colors.grey,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 10.0,
                horizontal: 20.0,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    text,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 15.0,
                    ),
                  ),
                  const SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    '${dtCreate.hour}:${dtCreate.minute}',
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 12.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
