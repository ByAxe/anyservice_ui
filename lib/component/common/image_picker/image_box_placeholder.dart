import 'package:anyserviceui/component/common/image_picker/image_box.dart';
import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ImageBoxPlaceholder extends StatelessWidget {
  const ImageBoxPlaceholder({
    Key key,
    this.active = false,
    this.onTap,
  })  : _icon =
            active ? FontAwesomeIcons.solidImages : FontAwesomeIcons.solidImage,
        _iconColor = active ? activeColor : inactivePrimaryColor,
        _backgroundColor = active ? activeColor : inactiveSecondaryColor,
        _decoration =
            active ? null : const BoxDecoration(color: inactiveSecondaryColor),
        super(key: key);

  final bool active;
  final VoidCallback onTap;

  static const Color activeColor = kCustomerAccentColor;
  static const Color inactivePrimaryColor = Colors.white;
  static const Color inactiveSecondaryColor = kLightGrey;

  final IconData _icon;
  final Color _iconColor;
  final Color _backgroundColor;
  final BoxDecoration _decoration;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: ImageBox(
        color: _backgroundColor,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 35),
          decoration: _decoration,
          child: Icon(_icon, color: _iconColor, size: 40.0),
        ),
      ),
    );
  }
}
