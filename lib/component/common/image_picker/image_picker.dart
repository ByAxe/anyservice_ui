import 'package:anyserviceui/bearer/assets_bearer.dart';
import 'package:anyserviceui/component/common/image_picker/fullscreenable_image_box.dart';
import 'package:anyserviceui/component/common/image_picker/image_box_placeholder.dart';
import 'package:anyserviceui/component/common/image_picker/removable_image_box.dart';
import 'package:anyserviceui/model/constants/application.dart';
import 'package:anyserviceui/model/constants/keys.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';

class ImagePicker extends StatefulWidget {
  @override
  _ImagePickerState createState() => _ImagePickerState();
}

class _ImagePickerState extends State<ImagePicker> {
  @override
  Widget build(BuildContext context) {
    final List<Asset> assets = Provider.of<AssetsBearer>(context).assets;

    // Create first thumbnail
    final Widget primaryThumbnail = ImageBoxPlaceholder(
      active: true,
      onTap: openGallery,
      key: Key(Keys.imagePickerBox),
    );

    final List<Widget> widgets = <Widget>[primaryThumbnail];

    if (assets.isNotEmpty) {
      // Add all chosen documents
      widgets.addAll(
        List<Widget>.generate(
          assets.length,
          (int index) {
            final Asset asset = assets[index];

            return RemovableImageBox(
              index: index,
              child: FullscreenableImageBox(
                tag: 'imageHero$index',
                asset: asset,
                child: AssetThumb(
                  asset: asset,
                  width: 300,
                  height: 300,
                ),
              ),
            );
          },
        ),
      );
    } else {
      // Add two grey rectangles
      widgets.add(ImageBoxPlaceholder(onTap: openGallery));
      widgets.add(ImageBoxPlaceholder(onTap: openGallery));
    }

    return ListView(
      scrollDirection: Axis.horizontal,
      children: widgets,
    );
  }

  /// Open gallery function
  Future<void> openGallery() async {
    final List<Asset> resultList = await MultiImagePicker.pickImages(
      maxImages: kOrderImagesCount,
      enableCamera: true,
    );

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    // Put assets into bearer
    Provider.of<AssetsBearer>(context, listen: false).addAssets(resultList);
  }
}
