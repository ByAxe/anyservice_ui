import 'package:anyserviceui/bearer/assets_bearer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class RemovableImageBox extends StatelessWidget {
  const RemovableImageBox({
    Key key,
    @required this.child,
    @required this.index,
  }) : super(key: key);

  final Widget child;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        child,
        IconButton(
          padding: const EdgeInsets.only(left: 95.0),
          icon: const Icon(
            FontAwesomeIcons.timesCircle,
            color: Colors.white,
          ),
          onPressed: () => Provider.of<AssetsBearer>(context, listen: false)
              .removeByIndex(index),
        ),
      ],
    );
  }
}
