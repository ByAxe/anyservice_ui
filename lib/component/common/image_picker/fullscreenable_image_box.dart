import 'package:anyserviceui/component/common/image_picker/image_box.dart';
import 'package:anyserviceui/screen/image_full_screen.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class FullscreenableImageBox extends StatelessWidget {
  const FullscreenableImageBox({
    Key key,
    @required this.asset,
    @required this.child,
    @required this.tag,
  }) : super(key: key);

  final Asset asset;
  final Widget child;
  final String tag;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute<ImageFullScreen>(
            builder: (_) {
              return ImageFullScreen(asset: asset, animationTag: tag);
            },
          ),
        );
      },
      child: ImageBox(
        child: Hero(
          tag: tag,
          child: child,
        ),
      ),
    );
  }
}
