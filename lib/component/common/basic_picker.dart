import 'dart:core';

import 'package:anyserviceui/model/constants/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';

class BasicPicker extends StatelessWidget {
  const BasicPicker({
    Key key,
    this.pickerData,
    this.onConfirm,
    this.fieldValue,
    this.textStyle = const TextStyle(color: Colors.black54, fontSize: 15.0),
    this.rightmostWidget,
    this.width,
  }) : super(key: key);

  final List<String> pickerData;
  final void Function(Picker, List<int>) onConfirm;
  final String fieldValue;
  final TextStyle textStyle;
  final Widget rightmostWidget;
  final double width;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => showPickerModal(context),
      child: Container(
        decoration: Styles.getBoxDecoration(),
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
        margin: const EdgeInsets.symmetric(vertical: 10.0),
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              fieldValue,
              style: textStyle,
            ),
            rightmostWidget ?? Container(),
          ],
        ),
      ),
    );
  }

  void showPickerModal(BuildContext context) {
    final Picker picker = Picker(
      adapter: PickerDataAdapter<String>(pickerdata: pickerData),
      changeToFirst: true,
      selectedTextStyle: const TextStyle(color: Colors.blue),
      onConfirm: onConfirm,
    );

    picker.showModal(context);
  }
}
