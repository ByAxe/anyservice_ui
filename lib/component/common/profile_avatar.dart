import 'package:anyserviceui/model/constants/routes.dart';
import 'package:flutter/material.dart';

import 'avatar.dart';

class ProfileAvatar extends StatelessWidget {
  const ProfileAvatar({@required this.photoUrl});

  final String photoUrl;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20.0),
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(context, userProfileRoute),
        child: Avatar(
          photoUrl: photoUrl,
          radius: 22.0,
        ),
      ),
    );
  }
}
