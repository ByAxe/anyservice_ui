import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/cupertino.dart';

class CurvedContainer extends Container {
  CurvedContainer({this.child});

  @override
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.longestSide,
      alignment: Alignment.topCenter,
      decoration: kSemiRoundedBoxDecoration,
      padding: const EdgeInsets.symmetric(
        vertical: 20.0,
        horizontal: 10.0,
      ),
      child: child,
    );
  }
}
