// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category(
    json['id'] as String,
    json['dtCreate'] == null
        ? null
        : DateTime.parse(json['dtCreate'] as String),
    json['dtUpdate'] == null
        ? null
        : DateTime.parse(json['dtUpdate'] as String),
    json['title'] as String,
    json['description'] as String,
    json['parentCategory'] == null
        ? null
        : Category.fromJson(json['parentCategory'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'id': instance.id,
      'dtCreate': instance.dtCreate?.toIso8601String(),
      'dtUpdate': instance.dtUpdate?.toIso8601String(),
      'title': instance.title,
      'description': instance.description,
      'parentCategory': instance.parentCategory,
    };
