// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'file.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FileMetadata _$FileMetadataFromJson(Map<String, dynamic> json) {
  return FileMetadata(
    id: json['id'] as String,
    dtCreate: json['dtCreate'] == null
        ? null
        : DateTime.parse(json['dtCreate'] as String),
    fileType: _$enumDecodeNullable(_$FileTypeEnumMap, json['fileType']),
    name: json['name'] as String,
    link: json['link'] as String,
    size: json['size'] as int,
    extension: json['extension'] as String,
  )..dtUpdate = json['dtUpdate'] == null
      ? null
      : DateTime.parse(json['dtUpdate'] as String);
}

Map<String, dynamic> _$FileMetadataToJson(FileMetadata instance) =>
    <String, dynamic>{
      'id': instance.id,
      'dtCreate': instance.dtCreate?.toIso8601String(),
      'dtUpdate': instance.dtUpdate?.toIso8601String(),
      'name': instance.name,
      'link': instance.link,
      'size': instance.size,
      'extension': instance.extension,
      'fileType': _$FileTypeEnumMap[instance.fileType],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$FileTypeEnumMap = {
  FileType.PROFILE_PHOTO: 'PROFILE_PHOTO',
  FileType.PORTFOLIO: 'PORTFOLIO',
  FileType.DOCUMENT: 'DOCUMENT',
  FileType.MAIN_ATTACHMENT: 'MAIN_ATTACHMENT',
  FileType.ATTACHMENT: 'ATTACHMENT',
};
