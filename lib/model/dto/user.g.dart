// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as String,
    dtCreate: json['dtCreate'] == null
        ? null
        : DateTime.parse(json['dtCreate'] as String),
    dtUpdate: json['dtUpdate'] == null
        ? null
        : DateTime.parse(json['dtUpdate'] as String),
    state: _$enumDecodeNullable(_$UserStateEnumMap, json['state']),
    role: _$enumDecodeNullable(_$UserRoleEnumMap, json['role']),
    verified: json['verified'] as bool,
    legalStatusVerified: json['legalStatusVerified'] as bool,
    name: json['name'] as String,
    email: json['email'] as String,
    issuer: json['issuer'] as String,
    picture: json['picture'] as String,
    passwordUpdateDate: json['passwordUpdateDate'] == null
        ? null
        : DateTime.parse(json['passwordUpdateDate'] as String),
    description: json['description'] as String,
    contacts: json['contacts'] as Map<String, dynamic>,
    legalStatus:
        _$enumDecodeNullable(_$LegalStatusEnumMap, json['legalStatus']),
    password: json['password'] as String,
    addresses: (json['addresses'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    ),
    defaultCountry: json['defaultCountry'] == null
        ? null
        : Country.fromJson(json['defaultCountry'] as Map<String, dynamic>),
    profilePhoto: json['profilePhoto'] == null
        ? null
        : FileMetadata.fromJson(json['profilePhoto'] as Map<String, dynamic>),
    documents: (json['documents'] as List)
        ?.map((e) =>
            e == null ? null : FileMetadata.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    portfolio: (json['portfolio'] as List)
        ?.map((e) =>
            e == null ? null : FileMetadata.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    listOfCountriesWhereServicesProvided:
        (json['listOfCountriesWhereServicesProvided'] as List)
            ?.map((e) =>
                e == null ? null : Country.fromJson(e as Map<String, dynamic>))
            ?.toList(),
  );
}

Map<String, dynamic> _$UserToJson(User instance) =>
    <String, dynamic>{
      'id': instance.id,
      'dtCreate': instance.dtCreate?.toIso8601String(),
      'dtUpdate': instance.dtUpdate?.toIso8601String(),
      'name': instance.name,
      'email': instance.email,
      'issuer': instance.issuer,
      'picture': instance.picture,
      'state': _$UserStateEnumMap[instance.state],
      'role': _$UserRoleEnumMap[instance.role],
      'verified': instance.verified,
      'legalStatusVerified': instance.legalStatusVerified,
      'passwordUpdateDate': instance.passwordUpdateDate?.toIso8601String(),
      'description': instance.description,
      'contacts': instance.contacts,
      'legalStatus': _$LegalStatusEnumMap[instance.legalStatus],
      'password': instance.password,
      'addresses': instance.addresses,
      'defaultCountry': instance.defaultCountry?.toJson(),
      'profilePhoto': instance.profilePhoto?.toJson(),
      'documents': instance.documents?.map((e) => e?.toJson())?.toList(),
      'portfolio': instance.portfolio?.map((e) => e?.toJson())?.toList(),
      'listOfCountriesWhereServicesProvided': instance
          .listOfCountriesWhereServicesProvided
          ?.map((e) => e?.toJson())
          ?.toList(),
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$UserStateEnumMap = {
  UserState.ACTIVE: 'ACTIVE',
  UserState.WAITING: 'WAITING',
  UserState.BLOCKED: 'BLOCKED',
};

const _$UserRoleEnumMap = {
  UserRole.ROLE_USER: 'ROLE_USER',
  UserRole.ROLE_ADMIN: 'ROLE_ADMIN',
  UserRole.ROLE_SUPER_ADMIN: 'ROLE_SUPER_ADMIN',
};

const _$LegalStatusEnumMap = {
  LegalStatus.LLC: 'LLC',
  LegalStatus.JSC: 'JSC',
};
