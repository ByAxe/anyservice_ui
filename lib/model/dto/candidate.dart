import 'package:anyserviceui/model/dto/api/aprimary.dart';
import 'package:anyserviceui/model/dto/chat.dart';
import 'package:anyserviceui/model/dto/response.dart';
import 'package:anyserviceui/model/dto/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'candidate.g.dart';

@JsonSerializable(explicitToJson: true)
class Candidate extends APrimary {
  Candidate({
    String id,
    DateTime dtCreate,
    DateTime dtUpdate,
    this.response,
    this.user,
    this.chat,
    this.selected,
  }) : super(id, dtCreate, dtUpdate);

  factory Candidate.fromJson(Map<String, dynamic> json) =>
      _$CandidateFromJson(json);

  Response response;
  User user;
  Chat chat;
  bool selected;

  @override
  Map<String, dynamic> toJson() => _$CandidateToJson(this);
}
