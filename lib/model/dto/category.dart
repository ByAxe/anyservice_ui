import 'package:anyserviceui/model/dto/api/aprimary.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

@JsonSerializable()
class Category extends APrimary {
  Category(
    String id,
    DateTime dtCreate,
    DateTime dtUpdate,
    this.title,
    this.description,
    this.parentCategory,
  ) : super(id, dtCreate, dtUpdate);

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  final String title;
  final String description;
  final Category parentCategory;

  @override
  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}
