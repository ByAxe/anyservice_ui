import 'package:anyserviceui/model/dto/api/aprimary.dart';
import 'package:json_annotation/json_annotation.dart';

part 'country.g.dart';

@JsonSerializable()
class Country extends APrimary {
  Country({
    String id,
    this.country,
    this.alpha2,
    this.alpha3,
    this.number,
  }) : super(id, null, null);

  factory Country.fromJson(Map<String, dynamic> json) =>
      _$CountryFromJson(json);

  final String country;
  final String alpha2;
  final String alpha3;
  final int number;

  @override
  Map<String, dynamic> toJson() => _$CountryToJson(this);
}
