import 'package:flutter/material.dart';

class Destination {
  const Destination({
    @required this.icon,
    @required this.title,
    @required this.modes,
    @required this.screen,
    @required this.id,
  });

  final String id;
  final IconData icon;
  final String title;
  final Widget Function(BuildContext context) screen;
  final List<Mode> modes;
}

/// The screens are only show in stated modes
// ignore: constant_identifier_names
enum Mode { CUSTOMER, WORKER, ADMINISTRATOR }
