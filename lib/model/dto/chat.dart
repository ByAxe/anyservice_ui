import 'package:anyserviceui/model/dto/api/aprimary.dart';
import 'package:anyserviceui/model/dto/message.dart';
import 'package:json_annotation/json_annotation.dart';

part 'chat.g.dart';

@JsonSerializable(explicitToJson: true)
class Chat extends APrimary {
  Chat({
    String id,
    DateTime dtCreate,
    DateTime dtUpdate,
    this.messages,
  }) : super(id, dtCreate, dtUpdate);

  factory Chat.fromJson(Map<String, dynamic> json) => _$ChatFromJson(json);

  List<Message> messages;

  @override
  Map<String, dynamic> toJson() => _$ChatToJson(this);
}
