// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) {
  return Order(
    json['id'] as String,
    json['dtCreate'] == null
        ? null
        : DateTime.parse(json['dtCreate'] as String),
    json['dtUpdate'] == null
        ? null
        : DateTime.parse(json['dtUpdate'] as String),
    json['headline'] as String,
    json['description'] as String,
    json['price'] == null ? null : DecimalWrapper.fromJson(json['price']),
    json['deadline'] == null
        ? null
        : DateTime.parse(json['deadline'] as String),
    json['location'] == null
        ? null
        : Location.fromJson(json['location'] as Map<String, dynamic>),
    _$enumDecodeNullable(_$OrderStateEnumMap, json['state']),
    json['responses'] as int,
    (json['categories'] as List)
        ?.map((e) =>
            e == null ? null : Category.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['phone'] as String,
    json['customer'] == null
        ? null
        : User.fromJson(json['customer'] as Map<String, dynamic>),
    (json['candidates'] as List)
        ?.map((e) =>
            e == null ? null : Candidate.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['mainAttachment'] == null
        ? null
        : FileMetadata.fromJson(json['mainAttachment'] as Map<String, dynamic>),
    (json['attachments'] as List)
        ?.map((e) =>
            e == null ? null : FileMetadata.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..currency = _$enumDecodeNullable(_$CurrencyEnumMap, json['currency']);
}

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'id': instance.id,
      'dtCreate': instance.dtCreate?.toIso8601String(),
      'dtUpdate': instance.dtUpdate?.toIso8601String(),
      'headline': instance.headline,
      'description': instance.description,
      'price': instance.price?.toJson(),
      'currency': _$CurrencyEnumMap[instance.currency],
      'deadline': instance.deadline?.toIso8601String(),
      'location': instance.location?.toJson(),
      'state': _$OrderStateEnumMap[instance.state],
      'responses': instance.responses,
      'categories': instance.categories?.map((e) => e?.toJson())?.toList(),
      'phone': instance.phone,
      'customer': instance.customer?.toJson(),
      'candidates': instance.candidates?.map((e) => e?.toJson())?.toList(),
      'mainAttachment': instance.mainAttachment?.toJson(),
      'attachments': instance.attachments?.map((e) => e?.toJson())?.toList(),
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$OrderStateEnumMap = {
  OrderState.DRAFT: 'DRAFT',
  OrderState.PUBLISHED: 'PUBLISHED',
  OrderState.EDITED: 'EDITED',
  OrderState.CANDIDATE_SELECTED: 'CANDIDATE_SELECTED',
  OrderState.COMPLETED: 'COMPLETED',
  OrderState.NOT_COMPLETED: 'NOT_COMPLETED',
  OrderState.CANCELLED: 'CANCELLED',
  OrderState.BLOCKED: 'BLOCKED',
  OrderState.REMOVED: 'REMOVED',
};

const _$CurrencyEnumMap = {
  Currency.USD: 'USD',
  Currency.BYN: 'BYN',
  Currency.PLN: 'PLN',
  Currency.EUR: 'EUR',
};
