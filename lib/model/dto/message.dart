import 'package:json_annotation/json_annotation.dart';

part 'message.g.dart';

@JsonSerializable(explicitToJson: true)
class Message {
  Message({this.senderId, this.text, this.status, this.dateTime});

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  String senderId;
  String text;
  MessageStatus status;
  DateTime dateTime;

  Map<String, dynamic> toJson() => _$MessageToJson(this);
}

enum MessageStatus {
  SENT,
  DELIVERED,
  READ,
}
