import 'package:json_annotation/json_annotation.dart';

part 'response.g.dart';

@JsonSerializable(explicitToJson: true)
class Response {
  Response({
    this.text,
    this.offeredPrice,
    this.priceUpdated,
  });

  factory Response.fromJson(Map<String, dynamic> json) =>
      _$ResponseFromJson(json);

  String text;
  String offeredPrice;
  bool priceUpdated;

  Map<String, dynamic> toJson() => _$ResponseToJson(this);
}
