import 'package:anyserviceui/model/dto/api/aprimary.dart';
import 'package:json_annotation/json_annotation.dart';

part 'file.g.dart';

@JsonSerializable()
class FileMetadata extends APrimary {
  FileMetadata({
    String id,
    DateTime dtCreate,
    this.fileType,
    this.name,
    this.link,
    this.size,
    this.extension,
  }) : super(id, dtCreate, null);

  factory FileMetadata.fromJson(Map<String, dynamic> json) =>
      _$FileMetadataFromJson(json);

  final String name;
  final String link;
  final int size;
  final String extension;
  final FileType fileType;

  @override
  Map<String, dynamic> toJson() => _$FileMetadataToJson(this);
}

enum FileType {
  PROFILE_PHOTO,
  PORTFOLIO,
  DOCUMENT,
  MAIN_ATTACHMENT,
  ATTACHMENT,
}

const Map<FileType, String> fileTypeEnumMap = <FileType, String>{
  FileType.PROFILE_PHOTO: 'PROFILE_PHOTO',
  FileType.PORTFOLIO: 'PORTFOLIO',
  FileType.DOCUMENT: 'DOCUMENT',
  FileType.MAIN_ATTACHMENT: 'MAIN_ATTACHMENT',
  FileType.ATTACHMENT: 'ATTACHMENT',
};
