import 'package:anyserviceui/core/decimal_wrapper.dart';
import 'package:anyserviceui/model/dto/api/aprimary.dart';
import 'package:anyserviceui/model/dto/candidate.dart';
import 'package:anyserviceui/model/dto/category.dart';
import 'package:anyserviceui/model/dto/file.dart';
import 'package:anyserviceui/model/dto/location.dart';
import 'package:anyserviceui/model/dto/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'order.g.dart';

@JsonSerializable(explicitToJson: true)
class Order extends APrimary {
  Order(
    String id,
    DateTime dtCreate,
    DateTime dtUpdate,
    this.headline,
    this.description,
    this.price,
    this.deadline,
    this.location,
    this.state,
    this.responses,
    this.categories,
    this.phone,
    this.customer,
    this.candidates,
    this.mainAttachment,
    this.attachments,
  ) : super(id, dtCreate, dtUpdate);

  Order.basic() : super(null, null, null);

  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);

  String headline;
  String description;
  DecimalWrapper price;
  Currency currency;
  DateTime deadline;
  Location location = Location();
  OrderState state;
  int responses;
  List<Category> categories;
  String phone;
  User customer;
  List<Candidate> candidates;

  FileMetadata mainAttachment;
  List<FileMetadata> attachments;

  @override
  Map<String, dynamic> toJson() => _$OrderToJson(this);
}

enum OrderState {
  DRAFT,
  PUBLISHED,
  EDITED,
  CANDIDATE_SELECTED,
  COMPLETED,
  NOT_COMPLETED,
  CANCELLED,
  BLOCKED,
  REMOVED,
}

extension ParseToString on Currency {
  String toShortString() {
    return toString().split('.').last;
  }
}

enum Currency {
  USD,
  BYN,
  PLN,
  EUR,
}
