// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Response _$ResponseFromJson(Map<String, dynamic> json) {
  return Response(
    text: json['text'] as String,
    offeredPrice: json['offeredPrice'] as String,
    priceUpdated: json['priceUpdated'] as bool,
  );
}

Map<String, dynamic> _$ResponseToJson(Response instance) => <String, dynamic>{
      'text': instance.text,
      'offeredPrice': instance.offeredPrice,
      'priceUpdated': instance.priceUpdated,
    };
