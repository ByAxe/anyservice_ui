// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'country.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Country _$CountryFromJson(Map<String, dynamic> json) {
  return Country(
    id: json['id'] as String,
    country: json['country'] as String,
    alpha2: json['alpha2'] as String,
    alpha3: json['alpha3'] as String,
    number: json['number'] as int,
  )
    ..dtCreate = json['dtCreate'] == null
        ? null
        : DateTime.parse(json['dtCreate'] as String)
    ..dtUpdate = json['dtUpdate'] == null
        ? null
        : DateTime.parse(json['dtUpdate'] as String);
}

Map<String, dynamic> _$CountryToJson(Country instance) => <String, dynamic>{
      'id': instance.id,
      'dtCreate': instance.dtCreate?.toIso8601String(),
      'dtUpdate': instance.dtUpdate?.toIso8601String(),
      'country': instance.country,
      'alpha2': instance.alpha2,
      'alpha3': instance.alpha3,
      'number': instance.number,
    };
