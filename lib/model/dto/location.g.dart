// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Location _$LocationFromJson(Map<String, dynamic> json) {
  return Location()
    ..country = json['country'] == null
        ? null
        : Country.fromJson(json['country'] as Map<String, dynamic>)
    ..city = json['city'] as String
    ..addressLine1 = json['addressLine1'] as String
    ..addressLine2 = json['addressLine2'] as String
    ..zipCode = json['zipCode'] as String
    ..locationType =
        _$enumDecodeNullable(_$LocationTypeEnumMap, json['locationType']);
}

Map<String, dynamic> _$LocationToJson(Location instance) => <String, dynamic>{
      'country': instance.country?.toJson(),
      'city': instance.city,
      'addressLine1': instance.addressLine1,
      'addressLine2': instance.addressLine2,
      'zipCode': instance.zipCode,
      'locationType': _$LocationTypeEnumMap[instance.locationType],
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$LocationTypeEnumMap = {
  LocationType.AT_CUSTOMER: 'AT_CUSTOMER',
  LocationType.AT_WORKER: 'AT_WORKER',
  LocationType.REMOTELY: 'REMOTELY',
  LocationType.ANYWHERE: 'ANYWHERE',
};
