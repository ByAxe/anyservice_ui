import 'package:anyserviceui/model/dto/country.dart';
import 'package:json_annotation/json_annotation.dart';

part 'location.g.dart';

@JsonSerializable(explicitToJson: true)
class Location {
  Location();

  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);

  Country country;
  String city;
  String addressLine1;
  String addressLine2;
  String zipCode;
  LocationType locationType;

  Map<String, dynamic> toJson() => _$LocationToJson(this);
}

const Map<LocationType, String> LocationTypeDescriptionMap =
    <LocationType, String>{
  null: 'Where the order should be executed?',
  LocationType.AT_CUSTOMER: 'At my location',
  LocationType.AT_WORKER: "At workers's location",
  LocationType.REMOTELY: 'Remotely',
  LocationType.ANYWHERE: 'Anywhere',
};

enum LocationType {
  AT_CUSTOMER,
  AT_WORKER,
  REMOTELY,
  ANYWHERE,
}
