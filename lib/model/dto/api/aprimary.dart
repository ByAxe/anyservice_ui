abstract class APrimary {
  APrimary(this.id, this.dtCreate, this.dtUpdate);

  String id;
  DateTime dtCreate;
  DateTime dtUpdate;

  Map<String, dynamic> toJson();
}
