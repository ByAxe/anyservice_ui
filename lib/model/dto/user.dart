import 'package:anyserviceui/model/dto/api/aprimary.dart';
import 'package:anyserviceui/model/dto/country.dart';
import 'package:anyserviceui/model/dto/file.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable(explicitToJson: true)
class User extends APrimary {
  User({
    String id,
    DateTime dtCreate,
    DateTime dtUpdate,
    this.state = UserState.ACTIVE,
    this.role = UserRole.ROLE_USER,
    this.verified = false,
    this.legalStatusVerified = false,
    this.name,
    this.email,
    this.issuer,
    this.picture,
    this.passwordUpdateDate,
    this.description,
    this.contacts = const <String, Object>{},
    this.legalStatus,
    this.password,
    this.addresses,
    this.defaultCountry,
    this.profilePhoto,
    this.documents,
    this.portfolio,
    this.listOfCountriesWhereServicesProvided,
  }) : super(id, dtCreate, dtUpdate);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  User.basic() : super(null, null, null);

  @JsonKey(ignore: true)
  FirebaseUser firebaseUser;

  String name;
  String email;
  String issuer;
  String picture;

  UserState state;
  UserRole role;
  bool verified;
  bool legalStatusVerified;

  DateTime passwordUpdateDate;
  String description;
  Map<String, Object> contacts = <String, Object>{};
  LegalStatus legalStatus;

  String password;
  Map<String, String> addresses;
  Country defaultCountry;
  FileMetadata profilePhoto;
  List<FileMetadata> documents;
  List<FileMetadata> portfolio;
  List<Country> listOfCountriesWhereServicesProvided;

  @override
  Map<String, dynamic> toJson() => _$UserToJson(this);

  void setPhone(String phone) {
    contacts ??= <String, Object>{};
    contacts['phone'] = phone;
  }

  String get phone => contacts != null ? contacts['phone']?.toString() : null;
}

enum UserState { ACTIVE, WAITING, BLOCKED }
enum UserRole { ROLE_USER, ROLE_ADMIN, ROLE_SUPER_ADMIN }
enum LegalStatus { LLC, JSC }
