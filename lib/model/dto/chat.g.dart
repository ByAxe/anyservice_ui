// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Chat _$ChatFromJson(Map<String, dynamic> json) {
  return Chat(
    id: json['id'] as String,
    dtCreate: json['dtCreate'] == null
        ? null
        : DateTime.parse(json['dtCreate'] as String),
    dtUpdate: json['dtUpdate'] == null
        ? null
        : DateTime.parse(json['dtUpdate'] as String),
    messages: (json['messages'] as List)
        ?.map((e) =>
            e == null ? null : Message.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ChatToJson(Chat instance) => <String, dynamic>{
      'id': instance.id,
      'dtCreate': instance.dtCreate?.toIso8601String(),
      'dtUpdate': instance.dtUpdate?.toIso8601String(),
      'messages': instance.messages?.map((e) => e?.toJson())?.toList(),
    };
