// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'candidate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Candidate _$CandidateFromJson(Map<String, dynamic> json) {
  return Candidate(
    id: json['id'] as String,
    dtCreate: json['dtCreate'] == null
        ? null
        : DateTime.parse(json['dtCreate'] as String),
    dtUpdate: json['dtUpdate'] == null
        ? null
        : DateTime.parse(json['dtUpdate'] as String),
    response: json['response'] == null
        ? null
        : Response.fromJson(json['response'] as Map<String, dynamic>),
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    chat: json['chat'] == null
        ? null
        : Chat.fromJson(json['chat'] as Map<String, dynamic>),
    selected: json['selected'] as bool,
  );
}

Map<String, dynamic> _$CandidateToJson(Candidate instance) => <String, dynamic>{
      'id': instance.id,
      'dtCreate': instance.dtCreate?.toIso8601String(),
      'dtUpdate': instance.dtUpdate?.toIso8601String(),
      'response': instance.response?.toJson(),
      'user': instance.user?.toJson(),
      'chat': instance.chat?.toJson(),
      'selected': instance.selected,
    };
