import 'package:flutter/material.dart';

const Color kErrorColor = Color(0xFFff666d);
const Color kDarkPrimaryColor = Color(0xFF303F9F);
const Color kPrimaryColor = Colors.indigo;
const Color kTextIconsColor = Colors.white;
const Color kCustomerAccentColor = Color(0xFF4E5FFF);
const Color kCustomerAccentColor0 = Color(0xFF1B2AC0);
const Color kCustomerAccentColor1 = Color(0xFF4E7FFF);
const Color kCustomerAccentColor2 = Color(0xFF4F3FFF);
const Color kCustomerAccentColor3 = Color(0xFF1A1EEE);
const Color kWorkerAccentColor = Color(0xFF26C97B);
const Color kPrimaryTextColor = Color(0xFF212121);
const Color kSecondaryTextColor = Color(0xFF757575);
const Color kDividerColor = Color(0xFFBDBDBD);
const Color kLightGrey = Color(0xFFDEDEDE);
