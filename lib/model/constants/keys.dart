class Keys {
  static String fab = 'fab';
  static String authButton = 'authButton';
  static String logoutButton = 'logoutButton';

  static String loginEmail = 'loginEmail';
  static String loginPassword = 'loginPassword';
  static String loginButton = 'loginButton';
  static String loginTextButton = 'loginTextButton';

  static String registrationPassword = 'registrationPassword';
  static String registrationEmail = 'registrationEmail';
  static String registrationName = 'registrationName';
  static String registrationPhone = 'registrationPhone';
  static String registrationRegisterButton = 'registrationRegisterButton';
  static String registrationTextButton = 'registrationTextButton';

  static String authLoginButton = 'authLoginButton';
  static String authRegisterButton = 'authRegisterButton';
  static String anonymousLoginButton = 'anonymousLoginButton';
  static String anonymousLoginTextButton = 'anonymousLoginTextButton';

  static String imagePickerBox = 'imagePickerBox';

  static String orderCreationHeadlineField = 'orderCreationHeadlineField';
  static String orderCreationCategoryField = 'orderCreationCategoryField';
  static String orderCreationDeadlineField = 'orderCreationDeadlineField';
  static String orderCreationPhoneNumber = 'orderCreationPhoneNumber';
  static String orderCreationLocationTypeField =
      'orderCreationLocationTypeField';
  static String orderCreationAddressField = 'orderCreationAddressField';
  static String orderCreationPriceField = 'orderCreationPriceField';
  static String orderCreationDiscussablePriceCheckBox =
      'orderCreationDiscussablePriceCheckBox';
  static String orderCreationDescriptionField = 'orderCreationDescriptionField';
  static String orderCreationButton = 'orderCreationButton';
  static String orderCreationCategoriesModalCloseButton =
      'orderCreationCategoriesModalCloseButton';
  static String orderCreationCategoriesModalAcceptButton =
      'orderCreationCategoriesModalAcceptButton';
  static String orderCreationCurrencyPicker = 'orderCreationCurrencyPicker';

  static String settingsMenuButton = 'settingsMenuButton';
  static String settingsCloseButton = 'settingsCloseButton';
}
