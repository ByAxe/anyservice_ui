class DestinationId {
  static String customerServices = 'customerServices';
  static String customerMyOrders = 'customerMyOrders';
  static String customerNotifications = 'customerNotifications';
  static String customerProfile = 'customerProfile';

  static String workerProfile = 'customerProfile';
  static String workerNotifications = 'workerNotifications';
  static String workerMyOrders = 'workerMyOrders';
  static String workerOrders = 'workerOrders';
}
