const String homeRoute = '/';
const String authRoute = '/auth';
const String loginRoute = '/login';
const String registrationRoute = '/registration';
const String userProfileRoute = '/profile';

const String supportRoute = '/support';
const String reportABugRoute = '/report_a_bug';
const String legalRoute = '/legal';
const String settingsRoute = '/settings';

const String customerNewOrderRoute = '/customer/newOrder';
const String customerServicesSearchRoute = '/customer/services/search';

const String workerNewServiceRoute = '/worker/newService';
const String workerOrdersSearchRoute = '/worker/orders/search';
