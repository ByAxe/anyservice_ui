import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/material.dart';

const TextStyle kSendButtonTextStyle = TextStyle(
  color: kCustomerAccentColor,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

const InputDecoration kMessageTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  hintText: 'Type your message here...',
  border: InputBorder.none,
);

const BoxDecoration kMessageContainerDecoration = BoxDecoration(
//  color: Color(0xffe8e8e8),
  border: Border(
    top: BorderSide(
      color: Color(0x553a5157),
      width: 1.0,
    ),
  ),
);

const TextStyle kButtonsTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 16.0,
);

const TextStyle kSingleButtonsTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 18.0,
  fontWeight: FontWeight.w300,
);

const InputDecoration kInputDecoration = InputDecoration(
  hintStyle: kH4TextStyle,
  contentPadding: EdgeInsets.symmetric(
    vertical: 10.0,
    horizontal: 20.0,
  ),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(32.0),
    ),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.white70,
      width: 1.0,
    ),
    borderRadius: BorderRadius.all(
      Radius.circular(32.0),
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.white70,
      width: 2.0,
    ),
    borderRadius: BorderRadius.all(
      Radius.circular(32.0),
    ),
  ),
  errorBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: kErrorColor,
      width: 2.0,
    ),
    borderRadius: BorderRadius.all(
      Radius.circular(32.0),
    ),
  ),
  errorStyle:
      TextStyle(color: kErrorColor, fontSize: 14, fontWeight: FontWeight.bold),
);

const TextStyle kMainMenuTextStyle = TextStyle(
  color: Colors.white,
);

const TextStyle optionStyle = TextStyle(
  fontSize: 30,
  fontWeight: FontWeight.bold,
);

const TextStyle kH1TextStyle = TextStyle(
  fontSize: 45.0,
  color: kTextIconsColor,
);

const TextStyle kH3TextStyle = TextStyle(
  fontSize: 18.0,
  color: Colors.white70,
);

const TextStyle kH3TextStyleLink = TextStyle(
  fontSize: 20.0,
  color: kTextIconsColor,
);

const TextStyle kH3TextStyleImportant = TextStyle(
  fontSize: 15.0,
  color: Colors.white70,
  fontWeight: FontWeight.bold,
);

const TextStyle kH4TextStyle = TextStyle(
  color: kTextIconsColor,
  fontSize: 15.0,
);

const TextStyle kH4TextStyleLink = TextStyle(
  fontSize: 15.0,
  fontWeight: FontWeight.bold,
  color: kTextIconsColor,
  decoration: TextDecoration.underline,
);

const TextStyle kH4TextStyleNotImportant = TextStyle(
  color: Colors.white70,
  fontSize: 15.0,
);

const TextStyle kFieldTextStyle = TextStyle(
  color: kDividerColor,
  fontSize: 15.0,
);

const BoxDecoration kSemiRoundedBoxDecoration = BoxDecoration(
  color: kTextIconsColor,
  borderRadius: BorderRadius.only(
    topLeft: Radius.circular(kStandardBorderRadiusSize),
    topRight: Radius.circular(kStandardBorderRadiusSize),
  ),
);

const BoxDecoration kAppGradient = BoxDecoration(
  gradient: RadialGradient(
    center: Alignment(-0.65, -0.65),
    stops: <double>[1, 1],
    colors: <Color>[kWorkerAccentColor, kCustomerAccentColor],
  ),
);

const double kStandardBorderRadiusSize = 25.0;

const Radius kPopupMenuBorderRadius = Radius.circular(
  kStandardBorderRadiusSize,
);
