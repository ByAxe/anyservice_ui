import 'package:anyserviceui/model/constants/palette.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_treeview/tree_view.dart';

class Styles {
  static InputDecoration getInputDecoration(String hintText) {
    return InputDecoration(
      hintStyle: kFieldTextStyle,
      hintText: hintText,
      contentPadding: const EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: 20.0,
      ),
      border: InputBorder.none,
    );
  }

  static InputDecoration getAuthInputDecoration({
    String hintText,
    IconData prefixIcon,
  }) {
    return kInputDecoration.copyWith(
      hintText: hintText,
      prefixIcon: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Icon(
          prefixIcon,
          color: kTextIconsColor,
        ),
      ),
    );
  }

  static BoxDecoration getBoxDecoration() {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: const BorderRadius.all(Radius.circular(32)),
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Colors.grey.withOpacity(0.2),
          spreadRadius: 3,
          blurRadius: 7,
        ),
      ],
    );
  }

  static TreeViewTheme getTreeViewTheme(BuildContext context) {
    return TreeViewTheme(
      expanderTheme: ExpanderThemeData(
        color: Colors.grey.shade800,
        size: 20,
      ),
      labelStyle: const TextStyle(
        fontSize: 16,
        letterSpacing: 0.3,
      ),
      parentLabelStyle: TextStyle(
        fontSize: 16,
        letterSpacing: 0.1,
        fontWeight: FontWeight.w800,
        color: Colors.blue.shade700,
      ),
      iconTheme: IconThemeData(
        size: 18,
        color: Colors.grey.shade800,
      ),
      colorScheme: Theme.of(context).brightness == Brightness.light
          ? ColorScheme.light(
              primary: Colors.blue.shade50,
              onPrimary: Colors.grey.shade900,
              background: Colors.transparent,
            )
          : const ColorScheme.dark(
              primary: Colors.black26,
              onPrimary: Colors.white,
              background: Colors.transparent,
              onBackground: Colors.white70,
            ),
    );
  }
}
