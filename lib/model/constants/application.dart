const bool kReleaseMode = false;
const int kOrderImagesCount = 10;
const double kOrderCreationScreenImagesBlockAspectRatio = 25 / 9;
const double kOrderCreationScreenBottomSpaceAspectRatio = 5 / 1;
