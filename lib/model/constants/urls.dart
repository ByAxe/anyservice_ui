const String kServerUrl = 'https://anyservice-275412.ew.r.appspot.com';
// const String kServerUrl = 'http://0.0.0.0:8080';
const String kUrlPrefix = '/api/v1';
const String kBackendUrl = kServerUrl + kUrlPrefix;

const String kUserUrl = '$kBackendUrl/user';
const String kCategoryUrl = '$kBackendUrl/category';
const String kChatUrl = '$kBackendUrl/chat';
const String kFileUrl = '$kBackendUrl/file';
const String kReviewUrl = '$kBackendUrl/review';
const String kCandidateUrl = '$kBackendUrl/candidate';
const String kOrderUrl = '$kBackendUrl/order';
const String kServiceUrl = '$kBackendUrl/service';
const String kCountryUrl = '$kBackendUrl/country';
