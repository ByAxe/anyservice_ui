import 'package:flutter/cupertino.dart';

class Data with ChangeNotifier {
  String _data = 'en';
  final Map<String, String> _obj = {
    'value1': 'ru',
    'value2': 'en',
  };

  String get getData => _data;

  String changeLang(String newString) {
    if (newString == 'en') {
      _data = _obj['value2'];
    } else if (newString == 'ru') {
      _data = _obj['value1'];
    }
    notifyListeners();
    return _data;
  }
}
