import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/model/dto/destination.dart';

class ModesMaps {
  static Map<Mode, String> modesActionRouteMap = <Mode, String>{
    Mode.CUSTOMER: customerNewOrderRoute,
    Mode.WORKER: workerNewServiceRoute,
//    Mode.ADMINISTRATOR: customerNewOrderRoute,
  };
}
