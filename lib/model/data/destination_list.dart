import 'package:anyserviceui/model/constants/destination_ids.dart';
import 'package:anyserviceui/model/dto/destination.dart';
import 'package:anyserviceui/screen/customer/bmenu/my_orders/customer_my_orders_screen.dart';
import 'package:anyserviceui/screen/customer/bmenu/notifications/customer_notifications_screen.dart';
import 'package:anyserviceui/screen/customer/bmenu/profile/customer_profile_screen.dart';
import 'package:anyserviceui/screen/customer/bmenu/services/customer_services_screen.dart';
import 'package:anyserviceui/screen/worker/bmenu/my_orders_screen.dart';
import 'package:anyserviceui/screen/worker/bmenu/notifications_screen.dart';
import 'package:anyserviceui/screen/worker/bmenu/profile_screen.dart';
import 'package:anyserviceui/screen/worker/bmenu/services_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

/// Class-bearer for destinations list
///
/// List contains all necessary data for bottom navigation menu
class DestinationList {
  DestinationList({this.context});

  final BuildContext context;

  List<Destination> getDestinations() => <Destination>[
        /// Customer destinations
        Destination(
          id: DestinationId.customerServices,
          icon: FontAwesomeIcons.search,
          title: 'Services',
          screen: CustomerServicesScreen().build,
          modes: <Mode>[Mode.CUSTOMER],
        ),
        Destination(
          id: DestinationId.customerMyOrders,
          icon: FontAwesomeIcons.listUl,
          title: 'My Orders',
          screen: CustomerMyOrdersScreen().build,
          modes: <Mode>[Mode.CUSTOMER],
        ),
        Destination(
          id: DestinationId.customerNotifications,
          icon: FontAwesomeIcons.bell,
          title: 'Notifications',
          screen: CustomerNotificationsScreen().build,
          modes: <Mode>[Mode.CUSTOMER],
        ),
        Destination(
          id: DestinationId.customerProfile,
          icon: FontAwesomeIcons.user,
          title: 'Profile',
          screen: CustomerProfileScreen().build,
          modes: <Mode>[Mode.CUSTOMER],
        ),

        /// Worker destinations
        Destination(
          id: DestinationId.workerOrders,
          icon: FontAwesomeIcons.search,
          title: 'Orders',
          screen: WorkerOrdersScreen().build,
          modes: <Mode>[Mode.WORKER],
        ),
        Destination(
          id: DestinationId.workerMyOrders,
          icon: FontAwesomeIcons.listUl,
          title: 'My Orders',
          screen: WorkerMyOrdersScreen().build,
          modes: <Mode>[Mode.WORKER],
        ),
        Destination(
          id: DestinationId.workerNotifications,
          icon: FontAwesomeIcons.bell,
          title: 'Notifications',
          screen: WorkerNotificationsScreen().build,
          modes: <Mode>[Mode.WORKER],
        ),
        Destination(
          id: DestinationId.workerProfile,
          icon: FontAwesomeIcons.user,
          title: 'Profile',
          screen: WorkerProfileScreen().build,
          modes: <Mode>[Mode.WORKER],
        ),
      ];
}
