class BackendException implements Exception {
  const BackendException(this.operation, this.statusCode, this.response);

  final Operation operation;
  final int statusCode;
  final String response;

  @override
  String toString() => '$operation operation - '
      ' Status code: $statusCode,'
      ' Response message: $response';
}

/// Standard set of BackEnd REST endpoints
enum Operation {
  CREATE,
  FIND_BY_ID,
  UPDATE,
  DELETE,
  FIND_ALL,
  EXISTS,
  COUNT,
  FIND_ALL_BY_IDS
}
