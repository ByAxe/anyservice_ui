import 'package:intl/intl.dart';
import 'package:regexpattern/regexpattern.dart';

// Helper functions
String enumToString(Object o) => o.toString().split('.').last;

T enumFromString<T>(String key, List<T> values) =>
    values.firstWhere((T v) => key == enumToString(v), orElse: () => null);

String convertDateTimeToString(DateTime dateTime) {
  final DateFormat formatter = DateFormat('dd.MM.yyyy');
  final String formatted = formatter.format(dateTime);

  return formatted;
}

T cast<T>(dynamic x) => x is T ? x : null;

extension StringExtensions on String {
  bool isAlphabetAndWhitespaceOnly() => RegVal.hasMatch(this, r'^[a-z A-Z]+$');
}