import 'package:anyserviceui/core/ui/device_screen_type.dart';
import 'package:flutter/material.dart';

/// Returns device screen type from given media query (context)
DeviceScreenType getDeviceScreenType(MediaQueryData mediaQuery) {
  final double deviceWidth = mediaQuery.size.shortestSide;

  // Mobile by default
  DeviceScreenType type = DeviceScreenType.MOBILE;

  if (deviceWidth > 950) {
    type = DeviceScreenType.DESKTOP;
  }

  if (deviceWidth > 600) {
    type = DeviceScreenType.TABLET;
  }

  return type;
}
