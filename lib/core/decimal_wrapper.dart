import 'package:decimal/decimal.dart';

class DecimalWrapper {
  DecimalWrapper(this.decimal);

  DecimalWrapper.fromDynamic(dynamic value) {
    decimal = DecimalWrapper.fromJson(value)?.decimal;
  }

  factory DecimalWrapper.fromJson(dynamic value) {
    Decimal result;

    if (value == null) {
      result = null;
    } else if (value is String) {
      result = Decimal.parse(value);
    } else if (value is double) {
      result = Decimal.parse(value.toString());
    } else if (value is int) {
      result = Decimal.fromInt(value);
    } else {
      throw Exception('Not supported source type');
    }

    return DecimalWrapper(result);
  }

  String toJson() => decimal.toString();

  @override
  String toString() => decimal.toString();

  Decimal decimal;

  DecimalWrapper operator +(DecimalWrapper second) {
    decimal += second?.decimal;
    return this;
  }

  DecimalWrapper operator -(DecimalWrapper second) {
    decimal -= second?.decimal;
    return this;
  }

  DecimalWrapper operator *(DecimalWrapper second) {
    decimal *= second?.decimal;
    return this;
  }

  DecimalWrapper operator /(DecimalWrapper second) {
    decimal /= second?.decimal;
    return this;
  }

  bool operator <(DecimalWrapper other) {
    return decimal < other?.decimal;
  }

  bool operator >(DecimalWrapper other) {
    return decimal > other?.decimal;
  }

  bool operator >=(DecimalWrapper other) {
    return decimal >= other?.decimal;
  }

  bool operator <=(DecimalWrapper other) {
    return decimal <= other?.decimal;
  }
}
