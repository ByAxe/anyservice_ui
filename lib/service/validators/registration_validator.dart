import 'package:anyserviceui/core/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:regexpattern/regexpattern.dart';

class RegistrationValidator {
  static String validateEmail(String value) {
    if (value.isEmpty) {
      return 'Current field cannot be empty';
    }
    if (!value.isEmail()) {
      return 'Not valid email format';
    }

    return null;
  }

  static String validatePassword(String value) {
    if (value.isEmpty) {
      return 'Current field cannot be empty';
    }
    if (!value.isPasswordNormal1()) {
      if (value.length < 8) {
        return 'Minimum length is 8 symbols';
      } else {
        return 'Password must contain at least: 1 letter & 1 number';
      }
    }
    return null;
  }

  static String validatePhone(String value) {
    if (value.isEmpty) {
      return 'Current field cannot be empty';
    }
    if (value.contains('-')) {
      return "Enter you phone without '-' symbols";
    }

    if (!value.isPhone()) {
      return 'Entered phone is not valid';
    }
    return null;
  }

  static String validateName(String value) {
    debugPrint(value);

    if (value.isEmpty) {
      return 'Current field cannot be empty';
    }
    if (!value.isAlphabetAndWhitespaceOnly()) {
      return 'Name should contain only letters and whitespaces';
    }

    return null;
  }
}
