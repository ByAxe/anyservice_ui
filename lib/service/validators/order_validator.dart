class OrderValidator {
  static String validateCategorySelection(dynamic value) {
    final List<String> categoryIdList = value as List<String>;

    if (categoryIdList == null || categoryIdList.isEmpty) {
      return 'Please select one or more options';
    }
    return null;
  }

  static String validatePrice(String price, {bool isDiscussablePrice}) {
    if (price.contains(RegExp('[^0-9.,]'))) {
      return 'Price can be only integer, or decimal with "." or ","';
    }

    if (!isDiscussablePrice && (price == null || price.isEmpty)) {
      return "Press 'Discussable price' or enter value";
    }

    return null;
  }

  static String validateDescription(String text) {
    if (text != null && text.length > 2000) {
      return 'Description exceeds maximum size';
    }
    return null;
  }

  static String validateShortDescription(String text) {
    if (text == null || text.isEmpty) {
      return 'Short description cannot be empty';
    }

    if (text.length < 3) {
      return 'Short description looks too short';
    }

    return null;
  }

  static String validateAddress(String address) {
    if (address == null || address.isEmpty) {
      return 'Address cannot not be empty';
    }

    return null;
  }

  static String validatePhoneNumber(String phoneNumber) {
    if (phoneNumber == null || phoneNumber.isEmpty) {
      return 'Phone cannot be empty';
    }

    return null;
  }
}
