import 'package:anyserviceui/model/dto/user.dart';
import 'package:anyserviceui/service/crud/api/acrud_service.dart';
import 'package:anyserviceui/service/crud/user_service.dart';
import 'package:anyserviceui/service/utility/security_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseAuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  ACrudService<User> userService;
  SecurityService securityService;
  bool _isLoggedIn = false;

  /// This method will be called on any user auth state changes in application
  Future<User> _processAuthentication(FirebaseUser firebaseUser) async {
    // If there is not user - return null or else set user to userService
    if (firebaseUser == null) {
      // show that user is not logged in
      _isLoggedIn = false;

      return null;
    }

    // Initialize security service
    securityService = SecurityService(getIdToken: firebaseUser.getIdToken);

    // Init user service
    userService = UserService(securityService);

    // Get user future via id from user service
    final User user = await userService.findById(firebaseUser.uid);

    // Set firebaseUser to user
    user?.firebaseUser = firebaseUser;

    // show that user is logged in
    _isLoggedIn = true;

    return user;
  }

  Stream<User> get onAuthStateChanged {
    return _auth.onAuthStateChanged.asyncMap(_processAuthentication);
  }

  Future<User> signInAnonymously() async {
    final AuthResult authResult = await _auth.signInAnonymously();
    return _processAuthentication(authResult.user);
  }

  /// Login user via its email and password
  Future<User> login(String email, String password) async {
    // Try login
    final AuthResult authResult = await _auth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );

    return _processAuthentication(authResult.user);
  }

  @Deprecated('Use [registerUser] operation')
  Future<void> registerWithEmailAndPassword(
      String email, String password) async {
    // Register user
    final AuthResult authResult = await _auth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );

    return _processAuthentication(authResult.user);
  }

  Future<void> registerUser(User user) async {
    userService = UserService(securityService);

    final User createdUser = await userService.create(user);
    await login(createdUser?.email, user?.password);
  }

  Future<void> signOut() async {
    // sign out
    await _auth.signOut();
  }

  bool get isUserLoggedIn => _isLoggedIn;
}
