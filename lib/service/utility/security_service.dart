import 'package:anyserviceui/model/constants/urls.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

/// Bears security related data
/// like tokens, necessary headers and etc.
class SecurityService {
  const SecurityService({@required this.getIdToken});

  static const String authKey = 'Authorization';
  static const String authValuePrefix = 'Bearer ';
  static const String securityUrl = '$kBackendUrl/user';

  final Future<IdTokenResult> Function({bool refresh}) getIdToken;

  /// Get security header (key-value pair)
  Future<Map<String, String>> getSecurityHeader({
    bool refreshToken = true,
  }) async {
    // Get object id token
    final IdTokenResult tokenResult = await getIdToken(refresh: refreshToken);

    // TODO what if user non logged in?...

    // Get token
    final String token = tokenResult.token;

    // Build special header value
    final String authValue = authValuePrefix + token;

    // return auth header
    return <String, String>{authKey: authValue};
  }
}
