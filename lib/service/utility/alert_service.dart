import 'package:anyserviceui/model/constants/palette.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class AlertService {
  void showAlert(Exception exception, BuildContext context) {
    Alert(
      context: context,
      style: AlertStyle(
        backgroundColor: kDarkPrimaryColor,
        descStyle: TextStyle(
          color: kTextIconsColor,
          fontSize: 15.0,
        ),
        titleStyle: TextStyle(
          color: kTextIconsColor,
          fontSize: 25.0,
          fontWeight: FontWeight.w600,
        ),
        alertBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
        ),
      ),
      title: 'Ошибка!',
      desc: exception.toString(),
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
          onPressed: () => Navigator.pop(context),
          color: kCustomerAccentColor,
          width: 120,
        )
      ],
    ).show();
  }
}
