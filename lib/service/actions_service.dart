import 'package:anyserviceui/model/constants/destination_ids.dart';
import 'package:anyserviceui/service/actions_builders/api/aactions_builder.dart';
import 'package:anyserviceui/service/actions_builders/basic_actions_builder.dart';
import 'package:anyserviceui/service/actions_builders/customer/customer_services_search_actions_builder.dart';
import 'package:anyserviceui/service/actions_builders/worker/worker_orders_search_actions_builder.dart';
import 'package:flutter/material.dart';

class ActionsService {
  const ActionsService({@required this.destinationId});

  final String destinationId;

  /// Returns actions list specific for each destination
  List<Widget> getActions(BuildContext context) {
    AActionsBuilder builder;

    if (DestinationId.customerServices == destinationId) {
      builder = CServicesSearchAB();
    } else if (DestinationId.workerOrders == destinationId) {
      builder = WOrdersSearchAB();
    } else {
      builder = BasicAB();
    }

    return builder.build(context);
  }
}
