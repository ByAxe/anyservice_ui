import 'package:anyserviceui/bearer/spinner_state_bearer.dart';
import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/model/dto/user.dart';
import 'package:anyserviceui/service/firebase_auth_service.dart';
import 'package:anyserviceui/service/utility/alert_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthHelper {
  static Future<void> signInAnonymously(BuildContext context) async {
    final FirebaseAuthService auth =
        Provider.of<FirebaseAuthService>(context, listen: false);
    final SpinnerStateBearer spinner =
        Provider.of<SpinnerStateBearer>(context, listen: false);

    spinner.toggleSpinner();

    try {
      await auth.signInAnonymously();

      // Hide spinner
      spinner.hideSpinner();

      Navigator.pushNamed(context, homeRoute);
    } on Exception catch (e) {
      final AlertService alertService =
          Provider.of<AlertService>(context, listen: false);

      // If error occurs - show it to the user
      alertService.showAlert(e, context);
    }

    // Hide spinner
    spinner.hideSpinner();
  }

  static Future<void> signIn(
    BuildContext context,
    GlobalKey<FormState> formKey,
    String email,
    String password,
  ) async {
    final FirebaseAuthService auth =
        Provider.of<FirebaseAuthService>(context, listen: false);
    final SpinnerStateBearer spinner =
        Provider.of<SpinnerStateBearer>(context, listen: false);

    // Stop if form is not valid
    if (!formKey.currentState.validate()) return;

    // Show spinner
    spinner.toggleSpinner();

    try {
      // Try to login
      await auth.login(email, password);

      // Hide spinner
      spinner.hideSpinner();

      // Go back to welcome screen (with auth data of a user in our hands)
      Navigator.pushNamed(context, homeRoute);
    } on Exception catch (e) {
      final AlertService alertService =
          Provider.of<AlertService>(context, listen: false);

      // If error occurs - show it to the user
      alertService.showAlert(e, context);
    }

    // Hide spinner
    spinner.hideSpinner();
  }

  static Future<void> registerUser(
    BuildContext context,
    GlobalKey<FormState> formKey,
    User user,
  ) async {
    final FirebaseAuthService auth =
        Provider.of<FirebaseAuthService>(context, listen: false);
    final SpinnerStateBearer spinner =
        Provider.of<SpinnerStateBearer>(context, listen: false);

    if (!formKey.currentState.validate()) return;
    // Show spinner
    spinner.toggleSpinner();

    try {
      await auth.registerUser(user);

      // Hide spinner
      spinner.hideSpinner();

      Navigator.pushNamed(context, homeRoute);
    } on Exception catch (e) {
      Provider.of<AlertService>(context, listen: false).showAlert(e, context);
    }

    // Hide spinner
    spinner.hideSpinner();
  }
}
