import 'package:anyserviceui/model/constants/urls.dart';
import 'package:anyserviceui/model/dto/category.dart';
import 'package:anyserviceui/service/crud/api/acrud_service.dart';
import 'package:anyserviceui/service/utility/security_service.dart';
import 'package:flutter/cupertino.dart';

class CategoryService extends ACrudService<Category> {
  const CategoryService(SecurityService securityService)
      : super(securityService);

  @override
  String get serviceUrl => kCategoryUrl;

  @override
  Category fromJson(Map<String, dynamic> json) => Category.fromJson(json);

  /// Converts list of category ids to List<Category>
  List<Category> categoryIdListToCategories({
    @required List<String> categoryIdList,
    @required List<Category> whereToSearch,
  }) {
    final List<Category> result = whereToSearch
        .where((Category category) => categoryIdList.contains(category.id))
        .toList();

    return result;
  }
}
