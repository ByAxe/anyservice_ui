import 'dart:convert';

import 'package:anyserviceui/model/dto/api/aprimary.dart';
import 'package:anyserviceui/model/exceptions/backend_exception.dart';
import 'package:anyserviceui/service/utility/security_service.dart';
import 'package:http/http.dart' as http;

/// Defines basic CRUD operations for all [DTO], that extend [APrimary]
abstract class ACrudService<DTO extends APrimary> {
  const ACrudService(this.securityService);

  final SecurityService securityService;

  /// Provide conversion from json to [DTO] instance
  DTO fromJson(Map<String, dynamic> json);

  /// Returns backend service url, associated with this [DTO]
  String get serviceUrl;

  /// Create [DTO]
  Future<DTO> create(DTO dto) async {
    final http.Response response = await http.post(
      serviceUrl,
      headers: await getHeadersCU(),
      body: json.encode(dto.toJson()),
    );

    // Add exception code and
    if (response.statusCode != 201) {
      throw BackendException(
        Operation.CREATE,
        response.statusCode,
        response.body,
      );
    }

    return convertJsonToDTO(response.body);
  }

  /// Update [DTO]
  /// [version] - dt update or dt create of dto that behaves like version of an object
  /// [id] - identifier of an object
  ///
  /// It is not possible to get these [version] and [id] from generic [DTO] so
  /// that is why they are explicitly passed as an arguments :(
  Future<DTO> update(DTO dto, DateTime version, String id) async {
    final String versionString = version.toIso8601String();

    final http.Response response = await http.put(
      '$serviceUrl/$id/version/$versionString',
      headers: await getHeadersCU(),
      body: json.encode(dto.toJson()),
    );

    // Add exception code and
    if (response.statusCode != 200) {
      throw BackendException(
        Operation.UPDATE,
        response.statusCode,
        response.body,
      );
    }

    return convertJsonToDTO(response.body);
  }

  /// Delete [DTO] by id
  Future<void> delete(DateTime version, String id) async {
    final String versionString = version.toIso8601String();

    final http.Response response = await http.delete(
      '$serviceUrl/$id/version/$versionString',
      headers: await securityService.getSecurityHeader(),
    );

    // Add exception code and
    if (response.statusCode != 200) {
      throw BackendException(
        Operation.DELETE,
        response.statusCode,
        response.body,
      );
    }
  }

  /// Find [DTO] by id
  Future<DTO> findById(String id) async {
    // Make get request to BE
    final http.Response response = await http.get(
      '$serviceUrl/$id',
      headers: await securityService.getSecurityHeader(),
    );

    if (response.statusCode != 200) {
      throw BackendException(
        Operation.FIND_BY_ID,
        response.statusCode,
        response.body,
      );
    }

    // Convert response to DTO
    return convertJsonToDTO(response.body);
  }

  /// Count all [DTO]
  Future<int> count() async {
    // Make get request to BE
    final http.Response response = await http.get(
      '$serviceUrl/count',
      headers: await securityService.getSecurityHeader(),
    );

    if (response.statusCode != 200) {
      throw BackendException(
        Operation.COUNT,
        response.statusCode,
        response.body,
      );
    }

    // Convert response to DTO
    return response.body as int;
  }

  /// Find out, whether [DTO] exists
  Future<bool> existsById(String id) async {
    // Make get request to BE
    final http.Response response = await http.get(
      '$serviceUrl/exists/$id',
      headers: await securityService.getSecurityHeader(),
    );

    if (response.statusCode != 200) {
      throw BackendException(
        Operation.EXISTS,
        response.statusCode,
        response.body,
      );
    }

    // Convert response to DTO
    return response.body as bool;
  }

  /// Find all [DTO]
  Future<List<DTO>> findAll() async {
    // Make get request to BE
    final http.Response response = await http.get(
      serviceUrl,
      headers: await securityService.getSecurityHeader(),
    );

    if (response.statusCode != 200) {
      throw BackendException(
        Operation.FIND_ALL,
        response.statusCode,
        response.body,
      );
    }

    // Convert response to list of DTO
    return _convertListJsonToListDTO(response.body);
  }

  /// Find all [DTO] by list of ids
  Future<List<DTO>> findAllByIdList(List<String> idList) async {
    // Make get request to BE
    final http.Response response = await http.get(
      '$serviceUrl/uuid/list/${idList.join(',')}',
      headers: await securityService.getSecurityHeader(),
    );

    if (response.statusCode != 200) {
      throw BackendException(
        Operation.FIND_ALL_BY_IDS,
        response.statusCode,
        response.body,
      );
    }

    // Convert response to list of DTO
    return _convertListJsonToListDTO(response.body);
  }

  /// Convert json to [DTO]
  DTO convertJsonToDTO(String json) {
    // Decode json string to Map<>
    final Map<String, dynamic> jsonMap =
        jsonDecode(json) as Map<String, dynamic>;

    // Convert json Map<> to DTO instance
    final DTO dto = fromJson(jsonMap);

    return dto;
  }

  /// Converts json list to list of [DTO]
  List<DTO> _convertListJsonToListDTO(String json) {
    if ('[]' == json || json == null) return <DTO>[];

    final List<dynamic> listAsJson = jsonDecode(json) as List<dynamic>;

    List<Map<String, dynamic>> listOfMaps = listAsJson
        ?.map((dynamic e) =>
            e == null ? null : Map<String, dynamic>.from(e as Map))
        ?.toList();

    final List<DTO> list =
        listOfMaps.map((Map<String, dynamic> dto) => fromJson(dto)).toList();

    return list;
  }

  /// Headers for CREATE and UPDATE operations
  Future<Map<String, String>> getHeadersCU() async {
    final Map<String, String> headers =
        await securityService?.getSecurityHeader() ?? <String, String>{};

    headers['Content-Type'] = 'application/json';

    return headers;
  }
}
