import 'package:anyserviceui/model/constants/urls.dart';
import 'package:anyserviceui/model/dto/order.dart';
import 'package:anyserviceui/service/crud/api/acrud_service.dart';
import 'package:anyserviceui/service/utility/security_service.dart';

class OrderService extends ACrudService<Order> {
  OrderService(SecurityService securityService) : super(securityService);

  @override
  Order fromJson(Map<String, dynamic> json) => Order.fromJson(json);

  @override
  String get serviceUrl => kOrderUrl;
}
