import 'dart:typed_data';

import 'package:anyserviceui/model/constants/urls.dart';
import 'package:anyserviceui/model/dto/file.dart';
import 'package:anyserviceui/model/exceptions/backend_exception.dart';
import 'package:anyserviceui/service/crud/api/acrud_service.dart';
import 'package:anyserviceui/service/utility/security_service.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class FileService extends ACrudService<FileMetadata> {
  FileService(SecurityService securityService) : super(securityService);

  @override
  FileMetadata fromJson(Map<String, dynamic> json) =>
      FileMetadata.fromJson(json);

  @override
  String get serviceUrl => kFileUrl;

  Future<FileMetadata> saveImage(Asset asset) async {
    final String fileType = fileTypeEnumMap[FileType.ATTACHMENT];

    // string to uri
    final Uri uri = Uri.parse('$kFileUrl/upload/$fileType');

    // create multipart request
    final MultipartRequest request = MultipartRequest('POST', uri)
      ..headers.addAll(await securityService.getSecurityHeader());

    final ByteData byteData = await asset.getByteData();
    final List<int> imageData = byteData.buffer.asUint8List();
    final String fileName = '${asset.identifier}.jpg'.replaceAll('/', '-');

    final MultipartFile multipartFile = MultipartFile.fromBytes(
      'file',
      imageData,
      filename: fileName,
      contentType: MediaType('image', 'jpg'),
    );

    // add file to multipart
    request.files.add(multipartFile);

    // send
    final Response response = await Response.fromStream(await request.send());

    // Check status
    if (response.statusCode != 200) {
      throw BackendException(
        Operation.CREATE,
        response.statusCode,
        response.body,
      );
    }

    // Return Created fileMetadata
    return super.convertJsonToDTO(response.body);
  }
}
