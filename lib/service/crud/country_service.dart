import 'package:anyserviceui/model/constants/urls.dart';
import 'package:anyserviceui/model/dto/country.dart';
import 'package:anyserviceui/service/crud/api/acrud_service.dart';
import 'package:anyserviceui/service/utility/security_service.dart';

class CountryService extends ACrudService<Country> {
  CountryService(SecurityService securityService) : super(securityService);

  @override
  Country fromJson(Map<String, dynamic> json) => Country.fromJson(json);

  @override
  String get serviceUrl => kCountryUrl;
}
