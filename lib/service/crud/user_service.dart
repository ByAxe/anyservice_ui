import 'package:anyserviceui/model/constants/urls.dart';
import 'package:anyserviceui/model/dto/user.dart';
import 'package:anyserviceui/service/crud/api/acrud_service.dart';
import 'package:anyserviceui/service/utility/security_service.dart';

class UserService extends ACrudService<User> {
  const UserService(SecurityService securityService) : super(securityService);

  @override
  String get serviceUrl => kUserUrl;

  @override
  User fromJson(Map<String, dynamic> json) => User.fromJson(json);
}
