import 'package:anyserviceui/model/constants/keys.dart';
import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:anyserviceui/service/actions_builders/api/aactions_builder.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BasicAB extends AActionsBuilder {
  @override
  List<Widget> build(BuildContext context) {
    return <Widget>[
      IconButton(
        iconSize: 25.0,
        icon: const Icon(FontAwesomeIcons.cog),
        onPressed: () => Navigator.pushNamed(context, settingsRoute),
        key: Key(Keys.settingsMenuButton),
      ),
      PopupMenuButton<Choice>(
        onSelected: (Choice choice) {
          // TODO Navigate to chosen location
        },
        itemBuilder: (BuildContext context) {
          return getChoices().map<PopupMenuItem<Choice>>((Choice choice) {
            return PopupMenuItem<Choice>(
              value: choice,
              child: ListTile(
                leading: Icon(choice.icon),
                title: Text(choice.title),
              ),
            );
          }).toList();
        },
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: kPopupMenuBorderRadius,
            bottomRight: kPopupMenuBorderRadius,
            topLeft: kPopupMenuBorderRadius,
          ),
        ),
      ),
    ];
  }

  /// Get all possible choices for popup menu
  List<Choice> getChoices() => const <Choice>[
        Choice(
            title: 'Support',
            route: supportRoute,
            icon: FontAwesomeIcons.headset),
        Choice(
            title: 'Report a bug',
            route: reportABugRoute,
            icon: FontAwesomeIcons.bug),
        Choice(
            title: 'Legal',
            route: legalRoute,
            icon: FontAwesomeIcons.pagelines),
      ];
}

class Choice {
  const Choice({this.title, this.route, this.icon});

  final String title;
  final String route;
  final IconData icon;
}
