import 'package:flutter/material.dart';

/// Abstract class that defines basic behavior for all actions builders
abstract class AActionsBuilder {
  /// Build method that returns list of widgets for an actions
  List<Widget> build(BuildContext context);
}
