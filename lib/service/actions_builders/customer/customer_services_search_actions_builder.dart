import 'package:anyserviceui/model/constants/routes.dart';
import 'package:anyserviceui/model/constants/style_constants.dart';
import 'package:anyserviceui/service/actions_builders/api/aactions_builder.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CServicesSearchAB extends AActionsBuilder {
  @override
  List<Widget> build(BuildContext context) {
    return <Widget>[
      IconButton(
        iconSize: kStandardBorderRadiusSize,
        padding: const EdgeInsets.only(right: 28.0),
        icon: const Icon(FontAwesomeIcons.search),
        onPressed: () => Navigator.pushNamed(
          context,
          customerServicesSearchRoute,
        ),
      ),
    ];
  }
}
